# NTEyeQ - Checking
## Systems Requirements
+ OS: Ubuntu
+ Environment: Nodejs
+ Database: MySql
## Installation
+ run bash script: install.sh
+ note: change password, username mysql if your database already exist in install folder
## Run
+ config: 
    + [dev.js](config/dev.js): config for development
    + [production.js](config/production.js): config for production environment
+ start project: npm start
+ test project: npm test


