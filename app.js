let express = require('express');
let path = require('path');
let redis = require("redis");
let cookieParser = require('cookie-parser');
let logger = require('morgan');
let bodyparser = require('body-parser');

let session = require('express-session');
let redisStore = require('connect-redis')(session);
let config = {};
let passport = require('passport'), flash = require('flash');
// var dash = require('appmetrics-dash');
// dash.attach();

if (process.env.NODE_ENV === 'live') {
  config = require('./config/production');
} else {
  config = require('./config/dev');
}

// let indexRouter = require('./routes/index');
let usersRouter = require('./routes/users');
let dashBoardRouter = require('./routes/dashboard');
let staffRouter = require('./routes/staffs');
let areaRouter = require('./routes/Area');
let routerIndex = require('./routes/router');

let client = redis.createClient();

let app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(session({
  secret: config.redis.secret,
  store: new redisStore({host: config.redis.host, port: config.redis.port, client: client, ttl: config.redis.TTL}),
  resave: false,
  saveUninitialized: true,
}));
app.use(flash());
app.use(logger('dev'));
app.use(bodyparser.json());
app.use(express.urlencoded({extended: true}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(passport.initialize());
app.use(passport.session());


app.use('', routerIndex.router);
// app.use('/users', usersRouter);
// app.use('/dashboard', dashBoardRouter);
// app.use('/staffs', staffRouter);
// app.use('/area', areaRouter);

// catch 404 and forward to error handler
app.use(function (req, res) {
  return res.status(404).json("Page not found");
});

// error handler
app.use(function (err, req, res) {
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
