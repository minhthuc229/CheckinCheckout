const io = require('socket.io');

class SocketServer {
  constructor(server) {
    this.io = io(server);
    let channels = [
      'user_size',
      'staff_size',
      'log_size',
      'users',
      'report',
      'staff',
      'log'
    ];
    this.io.on('connection', socket => {
      for (let i in channels) {
        let channel = channels[i];
        socket.on(channel, message => {
          this.io.emit(channel, message);
        })
      }
    })
  }

}

module.exports = SocketServer