module.exports = {
  sql: {
    port: 3306,
    database: "test3",
    host: "localhost",
    username: "root",
    password: "password",
  },
  redis: {
    host: "localhost",
    TTL: 3600,
    Secret: "NTQ-Solution",
    port: 6379
  },
  app: {
    port: 3000
  },
  privateKey: "NTQ-Solution"
}