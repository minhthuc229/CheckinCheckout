class AreaController {
  constructor(connection) {
    this.connection = connection;
    this.permissions = {
      INDEX: {Name: "Area index", Code: "AREAINDEX"},
      READ: {Name: "Area Search", Code: "AREAREAD"},
    }
    this.connection.permission.importsBatch(this.permissions);
  }

  index(req, res) {
    if (!req.user) return res.redirect('/');
    return res.render('../views/Staffs/index', {title: "Area", user: req.user});
  }

  async list(req, res) {
    if (!req.user) return res.status(404).json({});
    let result = await this.connection.area.list();
    res.send(result);
  }

}

module.exports = AreaController;