const connect = require('../model/Gateway/InitialConnect');
const StaticPage = require('./StaticPageController');
const User = require('./UsersController');
const Report = require('./ReportController');
const Staff = require('./StaffController');
const Area = require('./AreaController');
const RedisController = require('./RedisController');

var passport = require('passport'), auth = require('../helper/auth')
  , LocalStrategy = require('passport-local').Strategy;
let config = {};
if (process.env.NODE_ENV === 'live') {
  config = require('../config/production');
} else {
  config = require('../config/dev');
}

function Gateway() {
  this.db = new connect(config);
  let self = this;

  passport.serializeUser(function (user, done) {
    done(null, user);
  });

  passport.deserializeUser(async function (_user, done) {
    let user = await self.db.user.get(_user.Id);
    delete user.AuthenticatedPassword;
    if (user && user.AccessToken === _user.AccessToken) {
      return done(null, user);
    } else {
      return done("error", {});
    }
  });
}

Gateway.prototype.init = async function(){
  let self = this;
  await this.db.permission.list();
  self.redis = new RedisController(config);
  self.staticPage = new StaticPage(self.db);
  self.user = new User(self.db);
  self.report = new Report(self.db);
  self.staff = new Staff(self.db);
  self.area = new Area(self.db);
  self.PRIVATEKEY = config.privateKey;
  passport.use(new LocalStrategy({
      usernameField: 'UserName',
      passwordField: 'password'
    }, async function (username, password, done) {
      let user = await self.db.user.findUserName(username);
      if (user) {
        let _hashpass = auth.hash(password, user.salt);
        if (_hashpass === user.AuthenticatedPassword) {
          return done(null, user);
        } else {
          return done(null, false);
        }
      } else {
        return done(null, false);
      }
    }
  ));
}

module.exports = Gateway;