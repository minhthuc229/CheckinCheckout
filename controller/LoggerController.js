class LoggerController {
  constructor(connection) {
    this.connection = connection;
  }

  async create(req, res) {
    // authentication here
    if (!req.user) return res.status(500).json("Require Login");
    let params = req.body;
    let StaffId = params.StaffId, key = `Staff_${StaffId}_${params.AreaId}`, is_checkin = params.is_checkin;
    await this.connection.logger.insert(params);
    this.connection.redis.get(key).then(async value => {
      //if the staff not exist on redis
      if (value === null) {
        //check that record is checkin type otherwise skip
        if (is_checkin) {
          await this.connection.redis.set(key, params);
          return res.send({status: 1, message: 'inserted checkin into redis'});
        }
        return res.send({status: 0, message: 'abort just checkout'});
      } else {
        //if already exist on redis
        if (is_checkin) {
          await this.connection.redis.set(key, params);
          return res.send({status: 1, message: 'upserted checkin'})
        } else {
          let duration = params.CreatedOn - value.CreatedOn;
          let opts_loggerTimmer = {
            StaffId: params.StaffId,
            AreaId: params.AreaId,
            Year: params.Year,
            Month: params.Month,
            Day: params.Day
          }
          let timeLog = await this.connection.timeLogger.list(opts_loggerTimmer);
          timeLog = timeLog[0];
          if (timeLog) {
            timeLog.Duration += duration;
            let id = timeLog.Id;
            delete timeLog.Id;
            await this.connection.timeLogger.upsert(id, timeLog);
            this.connection.redis.delete(key);
            return res.send({status: 1, message: "Updated and remove queue!"})
          } else {
            timeLog = {...opts_loggerTimmer, Duration: duration};
            await this.connection.timeLogger.insert(timeLog);
            this.connection.redis.delete(key);
            return res.send({status: 1, message: "Inserted and remove queue!"})
          }

        }
      }
    }).catch(e => {
      return res.send({status: 0, message: e})
    })
  }
}

module.export = LoggerController;