const redis = require('redis');
class RedisController {
  constructor(config){
    this.client = redis.createClient(config.redis);
  }

  get(key){
    let self = this;
    return new Promise(((resolve, reject) => {
      self.client.get(key, (err, rs)=>{
        if (err) return reject(err);
        return resolve(rs);
      })
    }))
  }

  set(key, value){
    let self = this;
    return new Promise((resolve, reject) => {
      self.client.set(key, value, err=>{
        if (err) return reject(err);
        return resolve(true);
      })
    })
  }

  delete(key){
    this.client.del(key, (err, response)=>{
      if (err) throw Error(err);
      return response;
    })
  }
}
module.exports = RedisController;