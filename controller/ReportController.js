let helper = require('../helper/Sql')

class ReportController {
  constructor(connection) {
    this.connection = connection;
    this.permissions= {
      READ: {Name: "", Code: "READREPORT"}
    };
  }

  index(req, res) {
    if (!req.user) return res.redirect('/');
    return res.render('../views/DashBoard/index', {title: "DashBoard", user: req.user});
  }

  async api(req, res) {
    if (!req.user) return res.send({});
    let query = req.query;
    let user_count = this.connection.user.countAll();
    let logger_count = this.connection.log.countAll();
    let staff_count = this.connection.staff.countAll();
    let absent_count = this.connection.log.countAbsent(query);
    let early = this.connection.log.EarlyList({...query, limit: 5});
    let late = this.connection.log.LateList({...query, limit: 5});
    Promise.all([user_count, logger_count, staff_count, absent_count, early, late]).then(arr => {
      return res.send({
        size: {
          user: arr[0],
          log: arr[1],
          staff: arr[2],
          absent: arr[3],
        },
        early: arr[4],
        late: arr[5]
      })
    }).catch(e => {
      return res.status(501).json(e)
    })
  }

  async logStatistic(req, res) {
    if (!req.user) return res.send({});
    let query = req.query;
    if (!query) return res.send({});
    let target = req.query.Target;
    if (!target) return res.send({});
    if (!this.connection.log.targets.includes(target)) return res.send({});
    let rs = await this.connection.log.list(query);
    if (target === 'Gender' || target === 'AreaId') return res.send(rs);
    rs = helper.getListOfTime(target, rs, query);
    return res.send(rs)
  }

  async logList(req, res) {
    if (!req.user || parseInt(req.query.limit) > 100) return res.send([]);
    let limit = parseInt(req.query.limit);
    let rs = await this.connection.log.listDetailRecent({limit});
    rs = rs.map(r => {
      r.AccessAt = helper.objectTimeToString(r);
      return r;
    })
    return res.send(rs);
  }

  async search(req, res) {
    if (!req.user || parseInt(req.query.limit) > 100) return res.send([]);
    let rs = await this.connection.log.search(req.query);
    rs.data = rs.data.map(r => {
      r.AccessAt = helper.objectTimeToString(r);
      return r;
    })
    return res.send(rs);
  }

  async listArea(req, res) {
    if (!req.user) return res.send({});
    let query = req.query;
    let rs = await this.connection.log.areaLog(query);
    let object = {};
    for (let i = 0; i < rs.length; i++) {
      let item = rs[i];
      object[item.Id] = object[item.Id] ? object[item.Id] :
        {
          Male: item.Gender === 'Male' ? item.size : 0,
          Female: item.Gender === 'Female' ? item.size : 0,
          Name: item.AreaName
        };
      if (item.Gender === 'Male' && object[item.Id].Male === 0) object[item.Id].Male += item.size;
      if (item.Gender === 'Female' && object[item.Id].Female === 0) object[item.Id].Female += item.size;
    }
    let ar = [[],[],[]];
    for (let k in object){
      ar[0].push('Area ' + k);
      ar[1].push(object[k].Male);
      ar[2].push(object[k].Female);
    }
    // for ()
    return res.send(ar);
  }

  async absentList(req, res){
    if (!req.user) return res.send([]);
    let query = req.query;
    let rs = await this.connection.log.absentList(query);
    return res.send(rs);
  }

  async availableList(req, res){
    if (!req.user) return res.send([]);
    let query = req.query;
    let rs = await this.connection.log.availableList(query);
    return res.send(rs);
  }

  async StaffDetail(req, res){
    if (!req.user) return res.send([]);
    try {
      let details = await this.connection.log.listDetailOfStaff(req.query);
      return res.send(details);
    }catch (e) {
      console.log(e);
      return res.send([]);
    }

  }

  checkinIndex(req, res, next){
    if (!req.user) return next();
    return res.render('../views/Log/checkin', {title: "Log", subtitle: "Checkin", user: req.user, Log: "active open-menu", checkin:"active"})
  }


}

module.exports = ReportController;