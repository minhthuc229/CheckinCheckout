const moment = require('moment');

class UsersController {
  constructor(connection) {
    this.connection = connection;
  }

  index(req, res) {
    if (!req.user) return res.redirect('/');
    return res.render('../views/Staffs/index', {title: "Staffs", user: req.user});
  }

  async list(req, res) {
    if (!req.user) return res.status(404).json({});
    let limit = req.query.limit || 10, offset = req.query.offset || 0, fields = req.params.fields;
    let result = await this.connection.staff.listStaffs(limit, offset, fields);
    res.send(result);
  }

  addStaff(req, res) {
    if (!req.user) return res.status(404).json({});
    let user = req.body.staff;
    let self = this;
    user.Dob = moment(user.Dob, 'MM/DD/YYYY').unix();
    this.connection.staff.insert(user).then(async rs => {
      if (rs.code) {
        let count = await self.connection.staff.countAll();
        self.connection.staff.notify('staff_size', count);
        user.Id = rs.data;
        user.Dob = moment.unix(user.Dob).format('DD/MM/YYYY')
        user.CreatedOn = moment.unix(user.CreatedOn).format('DD/MM/YYYY')
        self.connection.staff.notify('staff', user);
        return res.send({
          status: 1,
          message: "inserted!"
        })
      } else {
        return res.send({
          status: 0,
          message: rs.data.sqlMessage
        })
      }
    }).catch(e => {
      console.log(e)
      return res.send({
        status: 0,
        message: e
      })
    })
  }

  async countAll(req, res) {
    if (!req.user) return res.status(404).json({});
    let size = await this.connection.staff.countAll();
    return res.send({size})
  }
}

module.exports = UsersController;