function StaticPageController(connection) {
  this.model = connection;
}

StaticPageController.prototype.login_index = function (req, res) {
  console.log("got here")
  if (req.user) {
    res.render('index', {title: 'DashBoard', user: req.user});
  } else {
    res.redirect('/login');
  }
}


StaticPageController.prototype.login = function (req, res) {
  if (req.user) return res.redirect("/");
  res.render('../views/StaticPage/Login', {title: "login"})
}


StaticPageController.prototype.logout = function (req, res) {
  req.logout();
  res.redirect('/login');
}

module.exports = StaticPageController;