const moment = require('moment');

class UsersController {
  constructor(connection) {
    this.connection = connection;
  }

  index(req, res) {
    if (!req.user) return res.redirect('/');
    return res.render('../views/Users/index', {title: "Users", user: req.user});
  }

  async list(req, res) {
    if (!req.user) return res.status(404).json({});
    let limit = req.query.limit || 10, offset = req.query.offset || 0, fields = req.query.fields;
    let result = await this.connection.user.listUsers(limit, offset, fields);
    result.data = result.data.map(rs => {
      rs.DOB = moment(rs.DOB, 'YYYY-MM-DDTHH:mm:ss.SSSZ').format('DD/MM/YYYY');
      rs.CreatedOn = moment.unix(rs.CreatedOn).format('DD/MM/YYYY HH:mm:ss');
      return rs;
    })
    res.send(result);
  }

  async user_search(req, res) {
    if (!req.user) return res.status(404).json({});
    let limit = req.query.limit || 10, offset = req.query.fields || 0, text = req.query.search;
    let result = await this.connection.user.generalSearch(limit, offset, text);
    return res.send(result);
  }

  async toggleStatus(req, res) {
    if (!req.user) return res.status(404).json({});
    let user_id = req.body.id, status = req.body.status;
    if (status === '1' || status === '0') {
      let current_user = await this.connection.user.get(user_id);
      if (current_user.Status === status) return res.send({
        status: 1,
        Message: `Successfully ${status === '1' ? 'activate' : 'disable'} ${current_user.UserName}!`
      })
      if (current_user.Status !== status) {
        // let update_user = {...current_user, Status: status};
        await this.connection.user.upsert(user_id, {Status: status});
        return res.send({
          status: 1,
          Message: `Successfully ${status === '1' ? 'activate' : 'disable'} ${current_user.UserName}!`
        })
      }
    } else {
      return res.send({status: 0, Message: "Invalid status!"});
    }

  }

  addUser(req, res){
    let user = req.body.user;
    user.password = "123456789";
    user.Dob = moment(user.Dob, 'YYYY-MM-DD').format('YYYY-MM-DD');
    if (!req.user) return res.status(404).json({});
    let self = this;
    this.connection.user.insert(user).then(async ()=>{
      let count = await self.connection.user.countAll();
      self.connection.user.notify('report', count)
      return res.send({
        status: 1,
        message: "inserted!"
      })
    }).catch(e=>{
      console.log(e)
      return res.send({
        status: 0,
        message: e
      })
    })
  }

  async countAll(req, res){
    if (!req.user) return res.status(404).json({});
    let size = await this.connection.user.countAll();
    return res.send({size})
  }
}

module.exports = UsersController;