const moment = require('moment');
module.exports = {
  objectToField: function (object) {
    let _result = {keys: [], values: []};
    for (let k in object) {
      _result.keys.push(k);
      _result.values.push(object[k]);
    }
    return {
      keys: _result.keys.join(', '),
      values: _result.values.join(', ')
    }
  },
  objectUpdateSql: function (object) {
    let _result = '', i = 0;
    for (let k in object) {
      _result += `${i === 0 ? '' : ','} ${k} = '${object[k]}'`;
      i++;
    }
    return _result;
  },
  diffObject: function (source, destination) {
    let final = {};
    for (let k in destination) {
      if (destination[k] !== source[k]) {
        final[k] = destination
      }
    }
    return final;
  },
  /**
   * @return {string}
   */
  ObjectToWhere: function (object, prefix) {
    let pre = prefix ? prefix + '.' : '';
    let string = [];
    for (let k in object) {
      let symbol = '=';
      let key = k;
      if (object[k] === '' || object[k] === null) continue;
      if (k.toLowerCase().includes('from')) {
        symbol = '>=';
        key = k.replace('from', '')
      }
      if (k.toLowerCase().includes('to') || k.toLowerCase().includes('end')) {
        symbol = '<=';
        key = k.replace('to', '').replace('end', '');
      }
      if (k.toLowerCase() === 'limit' || k.toLowerCase() === 'offset') continue;
      if (k.toLowerCase().includes('list')){
        symbol = 'in ';
        key = k.replace('List','').replace('list', '');
      }
      let is_string = isNaN(object[k]);
      string.push(`${pre}${key} ${symbol} ${is_string ? "'" + object[k]+"'" : object[k]}`);
    }
    return string.join(' And ');
  },
  capitalizeFirstLetter: function (string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
  },
  getResult: function (value) {
    return JSON.parse(JSON.stringify(value));
  },
  object2Array(object) {
    let ar1 = [], ar2 = [];
    for (let k in object) {
      ar1.push(k);
      ar2.push(object[k])
    }
    return [ar1, ar2];
  },
  timeToObject(milisecond) {
    let FormatDate = moment.unix(milisecond);
    return {
      Year: parseInt(FormatDate.format('YYYY')),
      Month: parseInt(FormatDate.format('MM')),
      Day: parseInt(FormatDate.format('DD')),
      Hour: parseInt(FormatDate.format('HH')),
      Minute: parseInt(FormatDate.format('mm')),
      Second: parseInt(FormatDate.format('ss')),
    }
  },
  objectTimeToString(object) {
    return `${object.Hour}:${object.Minute}:${object.Second} ${object.Day}/${object.Month}/${object.Year}`
  },
  getListOfTime(type, values, opts) {
    let object = {lable: [], values: []}, size = 0, addition = 0, prefix = '';
    switch (type) {
      case 'Minute': {
        size = 60;
        prefix = `${opts.Hour}:`
        break;
      }
      case 'Hour': {
        size = 24;
        break;
      }
      case 'Day': {
        size = moment(`${opts.Year}/${opts.Month}`, 'YYYY/MM').daysInMonth();
        addition = 1;
        break;
      }
      case 'Month': {
        size = 12;
        addition = 1;
        break;
      }
      default: {
        return {
          lable: [opts.Year],
          values: [values[opts.Year]]
        }
      }
    }
    for (let i = 0; i < size; i++) {
      object.lable.push(prefix + '' + ((i + addition) > 9 ? i + addition : '0' + (i + addition)));
      object.values.push(values[i + addition] || 0);
    }
    return object;

  }
}