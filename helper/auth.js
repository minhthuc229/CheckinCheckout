let crypto = require('crypto'),
  sjcl = require('sjcl'), iter = 1000;
// Parameter to hash, add sercurity...
const jwt = require('jsonwebtoken');
let auth = {};
let kdfbinary = function (password, salt, iterations, length) {
  length = length || 512;
  salt = sjcl.codec.utf8String.toBits(salt);
  let hash = sjcl.hash.sha256.hash(sjcl.hash.sha256.hash(password));
  let prff = function () {
    return new sjcl.misc.hmac(hash, sjcl.hash.sha1);
  };
  return sjcl.misc.pbkdf2(hash, salt, iterations, length, prff);
};
let sha256 = function (data) {
  return new Buffer.from(crypto.createHash('sha256').update(data).digest('binary'), 'binary');
};
let sha256sha256 = function (data) {
  return sha256(sha256(data));
};
auth.hash = function (password, salt) {
  let kdfb = kdfbinary(password, salt, iter);
  let kdfb64 = sjcl.codec.base64.fromBits(kdfb);
  let keyBuf = new Buffer.from(kdfb64);
  let passphrase = sha256sha256(keyBuf).toString('base64');
  return passphrase;
};

auth.saltGeneration = function(size){
  return crypto.randomBytes(size).toString("hex");
}

auth.sign = function(object, secret, expire=3600){
  return jwt.sign(object, secret, {expiresIn: expire});
}

auth.verify = async function(token, secrect, response, connection){
  jwt.verify(token, secrect, (err, res)=>{
    if (err) {
      response.redirect('/logout');
    }else{
      let _user = connection.db.user.get(res.Id);
      if (_user.AccessToken !== token){
        return response.redirect('/logout');
      }else {
        return res.Permissions;
      }
    }
  })
}
module.exports = auth;