OS=$(uname -s)
REV=$(uname -r)
MACH=$(uname -m)

GetVersionFromFile()
{
    VERSION=$(cat $1 |tr "\n" ' ' |sed s/.*VERSION.*=\ // )
}

if [ "${OS}" = "SunOS" ]; then
    OS=Solaris
    ARCH=$(uname -p)
    OSSTR="${OS} ${REV}(${ARCH} $(uname -v))"
elif [ "${OS}" = "AIX" ]; then
    OSSTR="${OS} $(oslevel) ($(oslevel -r))"
elif [ "${OS}" = "Linux" ]; then
    KERNEL=$(uname -r)
    if [ -f /etc/redhat-release ]; then
      rhelID=$(cat /etc/redhat-release |awk '{print $1}')
      if [ "${rhelID}" = "Red" ]; then
        DIST='RedHat'
        PSUEDONAME=$(cat /etc/redhat-release |sed s/.*\(// |sed s/\)//)
        REV=$(cat /etc/redhat-release |sed s/.*release\ // |sed s/\ .*//)
      elif [ "${rhelID}" = "CentOS" ]; then
        DIST='CentOS'
        PSUEDONAME=$(cat /etc/redhat-release |sed s/.*\(// |sed s/\)//)
        REV=$(cat /etc/redhat-release |sed s/.*release\ // |sed s/\ .*//)
      fi
    elif [ -f /etc/SuSE-release ]; then
        DIST=$(cat /etc/SuSE-release |tr "\n" ' '|sed s/VERSION.*//)
        REV=$(cat /etc/SuSE-release |tr "\n" ' ' |sed s/.*=\ //)
    elif [ -f /etc/mandrake-release ]; then
        DIST='Mandrake'
        PSUEDONAME=$(cat /etc/mandrake-release |sed s/.*\(// |sed s/\)//)
        REV=$(cat /etc/mandrake-release |sed s/.*release\ // |sed s/\ .*//)
    elif [ -f /etc/debian_version ]; then
      debianID=$(cat /etc/os-release |egrep -vi 'pretty|code' |grep -i name |cut -c6-50 |tr -d '"' |awk '{print $1}')
      if [ "${debianID}" = "Ubuntu" ]; then
        DIST="$(cat /etc/os-release |grep -i name |grep -i pretty |cut -c14-50 |tr -d '"')"
        REV=""
        chmod 755 install/ubuntu.sh
        ./install/ubuntu.sh
      elif [ "${debianID}" = "Debian" ]; then
        DIST="$(cat /etc/os-release |grep -i name |grep -i pretty |cut -c14-50 |tr -d '"')"
        REV=""
      fi
    elif [ -f /etc/gentoo-release ]; then
      DIST='Gentoo'
      PSUEDONAME=$(cat /etc/gentoo-release |sed s/.*\(// |sed s/\)//)
      REV=$(cat /etc/gentoo-release |sed s/.*release\ // |sed s/\ .*//)
    elif [ -f /etc/arch-release ]; then
      DIST='Arch'
      PSUEDONAME=$(cat /etc/arch-release |sed s/.*\(// |sed s/\)//)
      REV=$(cat /etc/arch-release |sed s/.*release\ // |sed s/\ .*//)
    fi
    if [ -f /etc/UnitedLinux-release ] ; then
        DIST="${DIST}[$(cat /etc/UnitedLinux-release |tr "\n" ' ' |sed s/VERSION.*//)]"
    fi

    OSSTR="${OS} ${DIST} ${REV}(${PSUEDONAME} ${KERNEL} ${MACH})"

fi

