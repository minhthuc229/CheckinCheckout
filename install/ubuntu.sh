#sudo apt-get update
#sudo apt-get install -y curl mysql-server
#curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -
#sudo apt-get install nodejs
#node -v
#npm -v
#sudo mysql_secure_installation utility
#sudo ufw enable
#sudo ufw allow mysql
#sudo systemctl start mysql
#sudo systemctl enable mysql
#sudo systemctl restart mysql

#======================#
# uncomment if you already install and set up your sql
# login in with user root and password is password
#======================
#/usr/bin/mysql -u root -ppassword -e "SELECT User, Host, authentication_string FROM mysql.user;"
#/usr/bin/mysql -u root -ppassword -e "UPDATE mysql.user SET authentication_string = PASSWORD('123456789') WHERE User = 'root';"
#/usr/bin/mysql -u root -p123456789 -e "FLUSH PRIVILEGES;"
/usr/bin/mysql -u root -ppassword -e "CREATE DATABASE test3;"
/usr/bin/mysql -u root -ppassword -e "SHOW DATABASES;"
/usr/bin/mysql -u root -ppassword -e "use test3; create table Area(Id int auto_increment primary key, Name varchar(256) not null, CreatedOn int not null, Address   varchar(256) null);"
/usr/bin/mysql -u root -ppassword -e "use test3; create table Menu(Id int auto_increment primary key, Code varchar(10) null, Name varchar(30) not null, constraint Menu_Code_uindex unique (Code));"
/usr/bin/mysql -u root -ppassword -e "use test3; create table Staff(    Id           int auto_increment primary key, Code         varchar(20)  null, FullName     varchar(20)  null,Dob          int          null,    CreatedOn    int          null,    MobileNumber varchar(13)  null,    Location     varchar(250) null,    Avatar       varchar(250) null,    Gender       varchar(10)  not null,    BornIn       int          null,    constraint Staff_Code_uindex        unique (Code));"
/usr/bin/mysql -u root -ppassword -e "use test3; create table Logger(    Id        int auto_increment        primary key,    StaffId   int        not null,    Year      int        not null,    Month     int        not null,    Day       int        not null,    Hour      int        null,    Minute    int        not null,    Second    int        not null,    CreatedOn int        not null,    AreaId    int        null,    CheckIn   tinyint(1) not null,    constraint Logger_Area_fk        foreign key (AreaId) references Area (Id),    constraint Logger_Staff_fk        foreign key (StaffId) references Staff (Id));"
/usr/bin/mysql -u root -ppassword -e "use test3; create index index_staff    on Logger (Year, Month, Day, Hour, Minute, Second);"
/usr/bin/mysql -u root -ppassword -e "use test3; create index Staff_Code_index    on Staff (Code);"
/usr/bin/mysql -u root -ppassword -e "use test3; create table Stranger(    Id        int auto_increment        primary key,    Year      int                  not null,    Month     int                  not null,    Day       int                  not null,    Hour      int                  not null,    Minute    int                  not null,    Second    int                  null,    AreaId    int                  null,    CreatedOn int                  not null,    Images    varchar(250)         not null,    CheckIn   tinyint(1) default 1 not null,    constraint Stranger_Area_fk        foreign key (AreaId) references Area (Id));"
/usr/bin/mysql -u root -ppassword -e "use test3; create table TimeLog(    Id         int auto_increment        primary key,    Year       int                  not null,    Month      int                  not null,    Day        int                  not null,    Duration   int                  not null,    IsCheckOut tinyint(1) default 0 not null,    AreaId     int                  not null,    StaffId    int                  null,    constraint TimeLog_Area_fk        foreign key (AreaId) references Area (Id),    constraint TimeLog_Staff_fk        foreign key (StaffId) references Staff (Id));"
/usr/bin/mysql -u root -ppassword -e "use test3; create table user(    Id                    int auto_increment        primary key,    UserName              varchar(20)          null,    DOB                   date                 null,    AuthenticatedPassword varchar(255)         null,    FirstName             varchar(20)          null,    LastName              varchar(20)          null,    salt                  varchar(20)          null,    CreatedOn             int                  null,    MobileNumber          varchar(30)          null,    Email                 varchar(100)         null,    AccessToken           varchar(20)          null,    ExpireDate            date                 null,    ResetToken            varchar(20)          null,    IsFirst               tinyint(1) default 1 not null,    Status                tinyint(1) default 1 not null,    constraint user_AccessToken_uindex        unique (AccessToken),    constraint user_Email_uindex        unique (Email),    constraint user_MobileNumber_uindex        unique (MobileNumber),    constraint user_UserName_uindex        unique (UserName));"
/usr/bin/mysql -u root -ppassword -e "use test3; create table Personalization(    Id             int auto_increment        primary key,    UserId         int            not null,    RpSummary      varchar(30)    null,    DashBoardLimit int default 10 not null,    constraint Personalization_User_fk        foreign key (UserId) references user (Id)            on delete cascade);"
/usr/bin/mysql -u root -ppassword -e "use test3; create table Permission(    Id   int auto_increment        primary key,    Name varchar(255) not null,    Code varchar(20)  not null);"
/usr/bin/mysql -u root -ppassword -e "use test3; show tables;"
npm i
node seed/SeedModel.js
node seed/SeedArea.js
node seed/SeedLogger.js
npm test
#npm start