const moment = require('moment');
              let baseModel = require('./BaseModel');
const schema = require('./validation/Schema');

const AreaSchema = {
  additionalProperties: false,
  require: ['Name'],
  properties: {
    Name: {type: 'string', maxLength: 100, minLength: 5},
    CreatedOn: {type: 'number'}
  }
}
schema.add('Area', AreaSchema);

class Area extends baseModel {
  constructor(opts, connection, status) {
    super(opts, connection, status);
    this.table = 'Area';
  }

  async insert(object) {
    let validator = schema.validator('Area');
    object.CreatedOn = moment().unix();
    if (!validator(object)) throw Error(schema.message(validator.errors));
    try {
      let rs = await super.insert(object);
      return rs;
    } catch (e) {
      return {status: 0, message: e}
    }
  }

  async list() {
    let sql = `Select * from ${this.table} Order by CreatedOn DESC`;
    let result = await this.query(sql);
    return result;
  }
}

module.exports = Area;