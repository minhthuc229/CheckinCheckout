const helperSQL = require('../helper/Sql');

class BaseModel {
  constructor(opt, connection, status = false) {
    this.connection = connection;
    this.status = status;
    this.socket = opt.socket;
    this.table = '';
  }

  get(id) {
    let self = this;
    return new Promise((resolve, reject) => {
      if (self.status) {
        let query = `Select * from ${self.table} where Id = ${id}`;
        self.connection.query(query, (err, result) => {
          if (err) return reject(err);
          if (Array.isArray(result) && result.length > 1) {
            return reject('Can not get your target because of your primary key is not unique!')
          } else if (result.length === 1) {
            return resolve(result[0])
          } else {
            return reject('Can not find any thing!')
          }
        })
      } else {
        return reject('Database did not connect');
      }
    })
  }

  insert(object) {
    let self = this;
    return new Promise((resolve, reject) => {
      self.connection.query(`Insert into ${self.table} Set ?`, object, (err, result) => {
        if (err) {
          return reject(err);
        }
        return resolve(result);
      })
    })
  }

  upsert(id, object) {
    let self = this;
    return new Promise((resolve, reject) => {
      self.connection.query(`Update ${self.table} SET ${helperSQL.objectUpdateSql(object)} where Id = ${id}`, (error, result) => {
        if (error) return reject(error);
        return resolve(result);
      })
    })
  }

  delete(id) {
    let self = this;
    return new Promise((resolve, reject) => {
      self.connection.query(`delete from ${self.table} where Id = ${id}`, (error, result) => {
        if (error) return reject(error);
        return resolve(result);
      })
    })
  }

  query(query) {
    let self = this;
    return new Promise((resolve, reject) => {
      self.connection.query(query, (error, result) => {
        if (error) {
          console.log(error)
          return reject("somethings it is wrong");
        }
        return resolve(helperSQL.getResult(result));
      })
    });
  }

  countAll() {
    let sql = `select count(*) as size from ${this.table}`;
    let self = this;
    return new Promise(((resolve, reject) => {
      self.connection.query(sql, (err, res) => {
        if (err) return reject(err);
        return resolve(res[0].size);
      })
    }))
  }

  notify(channel, data) {
    this.socket.emit(channel, data)
  }
}

module.exports = BaseModel;