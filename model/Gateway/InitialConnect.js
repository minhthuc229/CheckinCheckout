let Person = require('../User'), mysql = require('mysql');
let Staff = require('../Staff');
let Logger = require('../Logger');
let Area = require('../Area');
let Stranger = require('../Stranger');
let TimeLogger = require('../TimeLog');
let Permissions = require('../Permission');
let io = require('socket.io-client');


class InitialConnect {
  constructor(opts) {
    let config = opts.sql;
    let socket = io('http://localhost:3000');
    this.connection = mysql.createConnection({
      host: config.host,
      user: config.username,
      password: config.password,
      database: config.database,
      port: config.port
    });
    this.user = new Person({socket}, this.connection, true);
    this.staff = new Staff({socket}, this.connection, true);
    this.log = new Logger({socket}, this.connection, true);
    this.area = new Area({socket}, this.connection, true);
    this.stranger = new Stranger({socket}, this.connection, true);
    this.timeLogger = new TimeLogger({socket}, this.connection, true);
    this.permission = new Permissions({socket}, this.connection, true);
  }

  isConnected() {
    this.connection.connect(err => {
      this.connection.end();
      if (err) {
        console.log("error", err);
        return false;
      }else {
        console.log("database is connected");
        return true;
      }
    })
  }
}

module.exports = InitialConnect;