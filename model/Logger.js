const moment = require('moment');
let baseModel = require('./BaseModel');
const schema = require('./validation/Schema');
const helper = require('../helper/Sql');

const LoggerSchema = {
  additionalProperties: false,
  require: ['StaffId', 'Year', 'Month', 'Day', 'Hour', 'Minute', 'Second'],
  properties: {
    StaffId: {type: 'number'},
    Year: {type: 'number'},
    Month: {type: 'number', max: 12, min: 1},
    Day: {type: 'number', min: 1, max: 31},
    Hour: {type: 'number', min: 0, max: 23},
    Minute: {type: 'number', min: 0, max: 59},
    Second: {type: 'number', min: 0, max: 59},
    CreatedOn: {type: 'number'},
    CheckIn: {type: 'boolean'},
    AreaId: {type: 'number'},
  }
}
schema.add('Logger', LoggerSchema);

class Logger extends baseModel {
  constructor(opts, connection, status) {
    super(opts, connection, status);
    this.table = 'Logger';
    this.targets = ["Year", "Month", "Day", "Hour", "Minute", "Second", "Gender", "AreaId"];
  }

  async insert(object) {
    let validator = schema.validator('Logger');
    if (!validator(object)) throw Error(schema.message(validator.errors));
    object.CreatedOn = object.CreatedOn ? object.CreatedOn : moment().unix();
    try {
      let rs = await super.insert(object);
      return rs;
    } catch (e) {
      return {status: 0, message: e}
    }
  }

  async search(opts) {
    let sql = `Select L.Id, S.Code, S.FullName, L.Year, L.Month, L.Day, L.Hour, L.Minute, L.Second, S.Location, S.Avatar from Logger L inner join Staff S on L.StaffId = S.Id Where `;
    let count = `Select count(L.Id) as size from Logger L inner join Staff S on L.StaffId = S.Id Where `;
    let where = [];
    if (opts.fromId) where.push(`L.Id >= ${opts.fromId} `);
    if (opts.endId) where.push(`L.Id <= ${opts.endId} `);
    if (opts.Id) where.push(`L.Id = ${opts.Id} `);
    if (opts.fromYear) where.push(`L.Year >= ${opts.fromYear} `);
    if (opts.endYear) where.push(`L.Year <= ${opts.endYear} `);
    if (opts.Year) where.push(`L.Year = ${opts.Year} `);
    if (opts.fromMonth) where.push(`L.Month >= ${opts.fromMonth} `);
    if (opts.endMonth) where.push(`L.Month <= ${opts.endMonth} `);
    if (opts.Month) where.push(`L.Month = ${opts.Month} `);
    if (opts.fromDay) where.push(`L.Day >= ${opts.fromDay} `);
    if (opts.endDay) where.push(`L.Day <= ${opts.endDay} `);
    if (opts.Day) where.push(`L.Day = ${opts.Day} `);
    if (opts.fromHour) where.push(`L.Hour >= ${opts.fromHour} `);
    if (opts.endHour) where.push(`L.Hour <= ${opts.endHour} `);
    if (opts.Hour) where.push(`L.Hour = ${opts.Hour} `);
    if (opts.fromMinute) where.push(`L.Minute >= ${opts.fromMinute} `);
    if (opts.endMinute) where.push(`L.Minute <= ${opts.endMinute} `);
    if (opts.Minute) where.push(`L.Minute = ${opts.Minute} `);
    if (opts.custom) where.push(opts.custom);
    sql = sql + where.join(' And ');
    count = count + where.join(' And ');
    sql += ` Order by ${opts.orderBy || 'Id'} ${opts.order || 'DESC'} limit ${opts.Limit || 10} offset ${opts.Offset || 0}`;
    let rs = {};
    rs.size = await this.query(count);
    rs.data = await this.query(sql);
    return rs;
  }

  async list(opts) {
    let target = opts.Target;
    if (opts.Target === 'Year') {
      delete opts.Target;
      if (!opts.fromYear || !opts.range) throw Error('Missing params');
      return this.listYear(opts)
    } else {
      try {
        delete opts.Target;
        let obj = {}, sql = '';
        if (target === 'Gender') {
          sql = `select S.Gender, count(S.Gender) as size from Logger L inner join Staff S on L.StaffId = S.Id where ${helper.ObjectToWhere(opts, 'L')} group by Gender`;
        } else {
          sql = `Select ${target}, count(${target}) as size from ${this.table} where ${helper.ObjectToWhere(opts)} group by ${target}`;
        }
        let rs = await this.query(sql);
        for (let i = 0; i < rs.length; i++) {
          let item = rs[i];
          obj[item[target]] = item['size'];
        }
        return obj
      } catch (e) {
        console.log(e)
        return {}
      }
    }
  }

  async listYear(opts) {
    let from_year = opts.fromYear, to_year = opts.form_year + opts.range;
    let sql = `Select Year, count(Year) as size from ${this.table} where Year > ${from_year - 1} and Year < ${to_year + 1} group by Year`;
    try {
      let result = await this.query(sql);
      return result;
    } catch (e) {
      return {};
    }
  }

  async listDetailRecent(opts) {
    let limit = opts.limit || 10;
    let sql = `select S.Id, S.Code, S.FullName, L.Year, L.Month, L.Day, L.Hour, L.Minute, L.Second, S.Location, S.Avatar, L.CheckIn from Logger L inner join Staff S on L.StaffId = S.Id order by L.CreatedOn DESC limit ${limit}`;
    try {
      let rs = await this.query(sql);
      return rs;
    } catch (e) {
      console.log(e);
      return [];
    }
  }

  async listDetailOfStaff(opts) {
    let limit = opts.limit || 10;
    let offset = opts.offset || 0;
    let sql = `select S.Id, S.Code, S.FullName, L.Year, L.Month, L.Day, L.Hour, L.Minute, L.Second, S.Location, S.Avatar, L.CheckIn from Logger L inner join Staff S on L.StaffId = S.Id where ${helper.ObjectToWhere(opts, 'L')} order by L.CreatedOn DESC limit ${limit} offset ${offset}`;
    let count = `select count(*) as size from Logger L inner join Staff S on L.StaffId = S.Id where ${helper.ObjectToWhere(opts, 'L')}`;
    let rsc = await this.query(count);
    let rss = await this.query(sql);
    return {
      size: rsc[0].size,
      data: rss.map(rr => {
        rr.AccessAt = `${rr.Day}/${rr.Month} ${rr.Hour}:${rr.Minute}`;
        return rr
      })
    }
  }

  async areaLog(opts) {
    let query = `Select Gender, A.Name as AreaName, A.Id, count(AreaId) as size from Logger inner join Staff on Staff.Id = Logger.StaffId inner join Area A on Logger.AreaId = A.Id where ${helper.ObjectToWhere(opts, 'Logger')} group by AreaId, Gender`
    try {
      let rs = await this.query(query);
      return rs;
    } catch (e) {
      console.log(e);
      return {};
    }
  }

  async countAbsent(opts) {
    try {
      let query_count = `Select Count(S.Id) size from Staff S left join (Select S.Id, count(S.Id) as size from Staff S inner join Logger 
        L on S.Id = L.StaffId where ${helper.ObjectToWhere(opts, 'L')} group by L.StaffId) M on M.Id = S.Id where M.size is null`;
      let rs = await this.query(query_count);
      return rs[0].size;
    } catch (e) {
      console.log(e);
      return 0;
    }
  }

  async absentList(opts) {
    let queryList = `Select S.Id, S.FullName, S.Avatar, S.Code from Staff S left join (Select S.Id, count(S.Id) as size from Staff S inner join Logger 
        L on S.Id = L.StaffId where ${helper.ObjectToWhere(opts, 'L')} group by L.StaffId) M on M.Id = S.Id where M.size is null order by S.FullName limit ${opts.limit || 10} offset ${opts.offset || 0}`;
    let rs = await this.query(queryList);
    return rs;
  }

  async availableList(opts) {
    let queryList = `Select distinct S.Id, S.FullName, S.Code, concat(L.Hour, ':', L.Minute) as AccessAt, S.Location, S.Avatar from Staff S inner join Logger 
        L on S.Id = L.StaffId where ${helper.ObjectToWhere(opts, 'L')} order by S.Id limit ${opts.limit || 10} offset ${opts.offset || 0}`;
    let rs = await this.query(queryList);
    return rs;
  }

  async EarlyList(opts) {
    let query = `select distinct L.CreatedOn, S.FullName, S.Code, S.Location, S.Avatar, concat(L.Hour, ':', L.Minute) as AccessAt from Logger L inner join Staff S on S.Id = L.StaffId where ${helper.ObjectToWhere(opts, 'L')} order by CreatedOn ASC ${opts.limit ? 'limit ' + opts.limit : ''}`;
    let rs = await this.query(query);
    return rs;
  }

  async LateList(opts) {
    if (!opts) return [];
    let query = `select distinct L.CreatedOn, S.FullName, S.Code, S.Location, S.Avatar, concat(L.Hour, ':', L.Minute) as AccessAt from Logger L inner join Staff S on S.Id = L.StaffId where ${helper.ObjectToWhere(opts, 'L')} order by CreatedOn DESC ${opts.limit ? 'limit ' + opts.limit : ''}`;
    try {
      let rs = await this.query(query);
      return rs;
    } catch (e) {
      return [];
    }
  }
}

module.exports = Logger;