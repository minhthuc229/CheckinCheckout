let baseModel = require('./BaseModel');

class Permission extends baseModel{
  constructor(opts, connection, status){
    super(opts, connection, status);
    this.table = "Permission";
    this.permissions = [];
  }

  async register(object){
    if (!this.permissions.includes(object.Code)){
      let insert = await this.insert(object);
      // eslint-disable-next-line require-atomic-updates
      object.Id = insert;
      if (insert) this.permissions.push(object);
      return true;
    } else {
      throw Error("Duplicate permission!")
    }
  }

  async list(){
    let self = this;
    return new Promise(async (resolve, reject) => {
      try {
        let rs = await self.query(`Select Code from ${this.table}`);
        if (rs.length === 0) return resolve([]);
        rs = rs.map(r=>r.Code);
        this.permissions = rs;
        return resolve(rs)
      }catch(e){
        return reject(e);
      }
    })

  }

  async importsBatch(objects){
    try {
      for (let i in objects){
        let object = objects[i];
        return await this.register(object);
      }
    }catch (e) {
      return e;
    }
  }
}

module.exports = Permission;