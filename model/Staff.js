const moment = require('moment');
let baseModel = require('./BaseModel');
const schema = require('./validation/Schema');
const helper = require('../helper/Sql');

const StaffSchema = {
  additionalProperties: false,
  require: ['Code', 'FullName'],
  properties: {
    Code: {type: 'string', maxLength: 20, minLength: 5},
    FullName: {type: 'string', maxLength: 20, minLength: 5},
    Dob: {type: 'number'},
    CreatedOn: {type: 'number', default: moment().unix()},
    MobileNumber: {type: 'string', maxLength: 11},
    Location: {type: 'string'},
    Avatar: {type: 'string'},
    Gender: {type: 'string'},
    BornIn: {type: 'number'}
  }
}
schema.add('Staff', StaffSchema);

class Staff extends baseModel {
  constructor(opts, connection, status) {
    super(opts, connection, status);
    this.table = 'Staff';
  }

  async insert(object) {
    let validator = schema.validator('Staff');
    object.CreatedOn = moment().unix();
    object.Year = parseInt(moment.unix(object.Dob).format('YYYY'));
    if (!validator(object)) throw Error(schema.message(validator.errors));
    try {
      let a= await super.insert(object);
      return {
        code: 1,
        data: a
      }
    }catch (e) {
      return {
        code: 0, data: e
      }
    }
  }

  async findStaffCode(StaffCode) {
    let sql = `Select * from ${this.table} where Code = '${StaffCode}' limit 2`;
    try {
      let result = await this.query(sql);
      if (result.length) {
        result = helper.getResult(result[0]);
        return result;
      } else {
        return false
      }
    } catch (e) {
      return false;
    }
  }

  async listStaffs(limit, offset, fields) {
    let self = this;
    return new Promise((resolve, reject) => {
      let sql = '', count = '';
      if (!fields) {
        sql = `Select Id, Code, FullName, MobileNumber, Location, Avatar, CreatedOn, Dob from ${self.table} Order by Id DESC limit ${limit} offset ${offset}`;
        count = `Select count(Id) as size from ${self.table}`;
      } else {
        let select = `Select Id, Code, FullName, MobileNumber, Location, Email, Avatar, CreatedOn, Dob from ${self.table} Where `,
          where = helper.ObjectToWhere(fields);
        sql = select + where + `Order by Id DESC Limit ${limit} Offset ${offset}`;
        count = `Select count(Id) as size from ${self.table} Where ${where}`;
      }
      Promise.all([self.query(sql), self.query(count)]).then(value => {
        let sql_result = helper.getResult(value[0]);
        let count_result = helper.getResult(value[1])[0]['size'];
        if (sql_result && count_result) {
          return resolve({
            size: count_result,
            data: sql_result.map(d=>{
              d.Dob = moment.unix(d.Dob).format('DD/MM/YYYY');
              d.CreatedOn = moment.unix(d.CreatedOn).format('DD/MM/YYYY');
              return d;
            })
          })
        } else {
          return resolve({
            size: 0,
            data: []
          })
        }
      }).catch(e => {
        return reject(e)
      })
    });
  }

  async generalSearch(limit, offset, text){
    let sql = `Select Id, Code, FullName, MobileNumber, Location, Avatar from ${this.table} where StaffCode like '${text}%' or FullName like '${text}' limit ${limit} offset ${offset}`;
    let count = `select count(*) as size from ${this.table} where StaffCode like '${text}%' or FullName like '${text}'`;
    let size = await this.query(count);
    size = helper.getResult(size);
    if (size[0].size){
      let data = await this.query(sql);
      data = helper.getResult(data);
      return {
        size: size[0].size,
        data
      }
    }else {
      return {
        size: 0,
        data: []
      }
    }
  }
}

module.exports = Staff;