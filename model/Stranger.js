const moment = require('moment');
let baseModel = require('./BaseModel');
const schema = require('./validation/Schema');
const helper = require('../helper/Sql');

const StrangerSchema = {
  additionalProperties: false,
  require: ['Year', 'Month', 'Day', 'Hour', 'Minute', 'Second'],
  properties: {
    Year: {type: 'number', min: 2019},
    Month: {type: 'number', max: 12, min: 1},
    Day: {type: 'number', min: 1, max: 31},
    Hour: {type: 'number', min: 0, max: 23},
    Minute: {type: 'number', min: 0, max: 59},
    Second: {type: 'number', min: 0, max: 59},
    Images: {type: 'string'},
    AreaId: {type: 'number'},
    CreatedOn: {type: 'number'}
  }
}
schema.add('Stranger', StrangerSchema);

class Logger extends baseModel {
  constructor(opts, connection, status) {
    super(opts, connection, status);
    this.table = 'Stranger';
  }

  async insert(object) {
    let validator = schema.validator('Stranger');
    object.CreatedOn = moment().unix();
    if (!validator(object)) throw Error(schema.message(validator.errors));
    try {
      let rs = await super.insert(object);
      return rs;
    } catch (e) {
      return {status: 0, message: e}
    }
  }

  async search(opts){
    let sql = `Select L.Id, L.Year, L.Month, L.Day, L.Hour, L.Minute, L.Second, S.Location, S.Avatar from Logger L Where `;
    let count = `Select count(L.Id) as size from Logger L Where `;
    let where = [];
    if (opts.fromId) where.push(`L.Id >= ${opts.fromId} `);
    if (opts.endId) where.push(`L.Id <= ${opts.endId} `);
    if (opts.Id) where.push(`L.Id = ${opts.Id} `);
    if (opts.fromYear) where.push(`L.Year >= ${opts.fromYear} `);
    if (opts.endYear) where.push(`L.Year <= ${opts.endYear} `);
    if (opts.Year) where.push(`L.Year = ${opts.Year} `);
    if (opts.fromMonth) where.push(`L.Month >= ${opts.fromMonth} `);
    if (opts.endMonth) where.push(`L.Month <= ${opts.endMonth} `);
    if (opts.Month) where.push(`L.Month = ${opts.Month} `);
    if (opts.fromDay) where.push(`L.Day >= ${opts.fromDay} `);
    if (opts.endDay) where.push(`L.Day <= ${opts.endDay} `);
    if (opts.Day) where.push(`L.Day = ${opts.Day} `);
    if (opts.fromHour) where.push(`L.Hour >= ${opts.fromHour} `);
    if (opts.endHour) where.push(`L.Hour <= ${opts.endHour} `);
    if (opts.Hour) where.push(`L.Hour = ${opts.Hour} `);
    if (opts.fromMinute) where.push(`L.Minute >= ${opts.fromMinute} `);
    if (opts.endMinute) where.push(`L.Minute <= ${opts.endMinute} `);
    if (opts.Minute) where.push(`L.Minute = ${opts.Minute} `);
    if (opts.custom) where.push(opts.custom);
    sql = sql + where.join(' And ');
    count = count + where.join(' And ');
    sql += ` Order by ${opts.orderBy || 'Id'} ${opts.order || 'DESC'} limit ${opts.Limit || 10} offset ${opts.Offset || 0}`;
    let rs = {};
    rs.size = await this.query(count);
    rs.data = await this.query(sql);
    return rs;
  }

  /*
  * opts: <<Target, Year, Month, Day, Hour, Minute, Second>>
  * optional: if Year:
  *   <<from, to>>
  * */
  async list(opts) {
    let target = opts.Target;
    if (opts.Target === 'Year') {
      delete opts.Target;
      if (!opts.fromYear || !opts.range) throw Error('Missing params');
      return this.listYear(opts)
    } else {
      try {
        delete opts.Target;
        let obj = {};
        let sql = `Select ${target}, count(${target}) as size from ${this.table} where ${helper.ObjectToWhere(opts)} group by ${target}`;
        let rs = await this.query(sql);
        for (let i = 0 ; i < rs.length; i++){
          let item = rs[i];
          obj[item[target]] = item['size'];
        }
        return obj
      } catch (e) {
        console.log(e)
        return {}
      }
    }
  }

  async listYear(opts) {
    let from_year = opts.fromYear, to_year = opts.form_year + opts.range;
    let sql = `Select Year, count(Year) as size from ${this.table} where Year > ${from_year - 1} and Year < ${to_year + 1} group by Year`;
    try {
      let result = await this.query(sql);
      return result;
    } catch (e) {
      return {};
    }
  }

  async listDetailRecent(opts){
    let limit = opts.limit;
    let sql = `select L.Id, S.Code, S.FullName, L.Year, L.Month, L.Day, L.Hour, L.Minute, L.Second, S.Location, S.Avatar from Logger L inner join Staff S on L.StaffId = S.Id order by L.Id DESC limit ${limit}`;
    try {
      let rs = await this.query(sql);
      return rs;
    }catch (e) {
      console.log(e);
      return [];
    }
  }
}

module.exports = Logger;