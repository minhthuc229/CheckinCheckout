let baseModel = require('./BaseModel');
const schema = require('./validation/Schema');
const sqlhelper = require('../helper/Sql')

const TimeLogSchema = {
  additionalProperties: false,
  require: ['StaffId','AreaId'],
  properties:{
    Year:{type:'number'},
    Month:{type: 'number'},
    Day:{type: 'number'},
    StaffId:{type: 'number'},
    AreaId:{type: 'number'},
    Duration:{type: 'number'},
    IsCheckIn:{type:'boolean'},
  }
}
schema.add('TimeLogger', TimeLogSchema);

class TimeLog extends baseModel{
  constructor(opts, connection, status){
    super(opts, connection, status);
    this.socket = opts.socket;
    this.table = "TimeLog";
  }

  insert(object) {
    let validator = schema.validator('TimeLogger');
    if (!validator(object)) throw Error(schema.message(validator.errors));
    return super.insert(object);
  }

  async list(opts){
    let query = `select * from ${this.table} where ${sqlhelper.ObjectToWhere(opts)}`;
    try {
      let rs = await this.query(query);
      return rs;
    }catch (e) {
      console.log(e)
      return [];
    }
  }
}
module.exports = TimeLog;