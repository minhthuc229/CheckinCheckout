const moment = require('moment');
let baseModel = require('./BaseModel');
const schema = require('./validation/Schema');
const auth = require('../helper/auth');
const helper = require('../helper/Sql');

const PersonSchema = {
  additionalProperties: false,
  require: ['UserName', 'PassWord'],
  properties: {
    UserName: {type: 'string', maxLength: 20, minLength: 5},
    PassWord: {type: 'string', maxLength: 20, minLength: 5},
    Dob: {type: 'string', format: 'date'},
    FirstName: {type: 'string', minLength: 1, maxLength: 20},
    LastName: {type: 'string', minLength: 1, maxLength: 20},
    salt: {type: 'string'},
    CreatedOn: {type: 'number'},
    MobileNumber: {type: 'string', maxLength: 30},
    Email: {type: 'string', format: 'email', maxLength: 120, minLength: 12},
    AccessToken: { type: 'string', maxLength: 20 }
  }
}
schema.add('User', PersonSchema);

class User extends baseModel {
  constructor(opts, connection, status) {
    super(opts, connection, status);
    this.socket = opts.socket;
    this.table = 'user';
  }

  async insert(object) {
    let validator = schema.validator('User');
    let salt = auth.saltGeneration(10);
    object.salt = salt;
    object.PassWord = '' + object.PassWord;
    let AuthenticatedPassword = auth.hash(object.PassWord, salt);
    if (!validator(object)) throw Error(schema.message(validator.errors));
    delete object.PassWord;
    object.CreatedOn = moment().unix();
    object.AuthenticatedPassword = AuthenticatedPassword;
    object.AccessToken = auth.saltGeneration(10);
    object.IsFirst = true;
    return await super.insert(object);
  }

  async findUserName(username) {
    let sql = `Select * from ${this.table} where UserName = '${username}' limit 2`;
    try {
      let result = await this.query(sql);
      if (result.length) {
        return result[0];
      } else {
        return false
      }
    } catch (e) {
      return false;
    }
  }

  async listUsers(limit, offset, fields) {
    let self = this;
    return new Promise((resolve, reject) => {
      let sql = '', count = '';
      if (!fields) {
        sql = `Select Id, UserName, CreatedOn, DOB, FirstName, LastName, ExpireDate, Status from ${self.table} Order by CreatedOn DESC limit ${limit} offset ${offset}`;
        count = `Select count(Id) as size from ${self.table}`;
      } else {
        let select = `Select Id, UserName, CreatedOn, DOB, FirstName, LastName, ExpireDate, Status from ${self.table} Where `,
          where = helper.ObjectToWhere(fields);
        sql = select + where + `Order by Id DESC Limit ${limit} Offset ${offset}`;
        count = `Select count(Id) as size from ${self.table} Where ${where}`;
      }
      Promise.all([self.query(sql), self.query(count)]).then(value => {
        let sql_result = helper.getResult(value[0]);
        let count_result = helper.getResult(value[1])[0]['size'];
        if (sql_result && count_result) {
          return resolve({
            size: count_result,
            data: sql_result
          })
        } else {
          return resolve({
            size: 0,
            data: []
          })
        }
      }).catch(e => {
        return reject(e)
      })
    });
  }

  async generalSearch(limit, offset, text){
    let sql = `Select Id, UserName, CreatedOn, DOB, FirstName, LastName, ExpireDate, Status from ${this.table} where UserName like '${text}%' limit ${limit} offset ${offset}`;
    let count = `select count(*) as size from ${this.table} where UserName like '${text}%'`;
    let size = await this.query(count);
    size = helper.getResult(size);
    if (size[0].size){
      let data = await this.query(sql);
      data = helper.getResult(data);
      return {
        size: size[0].size,
        data
      }
    }else {
      return {
        size: 0,
        data: []
      }
    }
  }
}

module.exports = User;