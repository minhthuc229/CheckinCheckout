const Ajv = require('ajv');
const ajv = new Ajv({allErrors: true,  removeAdditional: true, jsonPointers: true});
require('ajv-errors')(ajv);
var schemas = {}
var _validators = {
}
var ignore = function () {
  return false;
}

module.exports = {
  schema: schemas,
  validator: function (model) {
    if (_validators[model]) {
      return _validators[model];
    }
    return ignore;
  },
  add: function (model, specs, refs) {
    if (refs){
      ajv.addSchema(refs);
    }
    _validators[model] = ajv.compile(specs);
  },
  compile: function (specs) {
    return ajv.compile(specs);
  },
  message: function (errors) {
    var message = "";
    for (var i = 0; i < errors.length; i++) {
      // message += errors[i].dataPath + " " + errors[i].message + "\n";
      message += errors[i].message + "\n";
    }
    return message;
  }
};
