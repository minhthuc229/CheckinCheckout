// eslint-disable-next-line no-undef
$(document).ready(() => {
  let nowDate = new Date();

  function convertDate(date) {
    date = new Date(date);
    return date.getTime() / 1000;
  }

  $(function () {
    let app = new Vue({
      el: '#app',
      data: {
        top_pagination: true,
        visible_setting: false,
        chartLoad: true,
        limit: 10,
        offset: 0,
        src: '',
        dialogVisible: false,
        visible: false,
        title: '',
        from_time: '',
        dateRange: [nowDate.setMonth(nowDate.getMonth() - 1), new Date],
        size: {
          user: 0,
          staff: 0,
          log: 0,
          just: 0,
          absent: 0,
          available: 0
        },
        justNow: [],
        early: [],
        late: [],
        chartRs: {lable: [], values: []},
        chartOpt: {},
        staffs: [],
        type: 'Day',
        dialogType: '',
        types: [
          {label: "Minutes", value: "Minute"},
          {label: "Hours", value: "Hour"},
          {label: "Days", value: "Day"},
          {label: "Months", value: "Month"},
          {label: "Years", value: "Year"},
        ],
        init_logger_justnow: 0,
        last_logger_justnow: 0,
        justnow_size: 0,
        radar: {
          CreatedOn: []
        },
        areas_list: [],
        settings: {
          areas: [],
          fromCreatedOn: 0,
          toCreatedOn: 0
        },
        charts: {
          line: {},
          pie: {},
          radar: {}
        }
      },
      methods: {
        applySetting() {
          this.settings.fromCreatedOn = convertDate(this.dateRange[0]);
          this.settings.toCreatedOn = convertDate(this.dateRange[1]);
          this.charts.line.destroy();
          this.charts.pie.destroy();
          this.charts.radar.destroy();
          this.mountList(this.type, true)
        },
        available(){
          this.top_pagination = this.limit > 10 && this.justnow_size > 0;
          this.dialogType = 'available';
          this.chartLoad = true;
          this.title = `List of staffs available today`;
          let self = this;
          $.get('/dashboard/available', {
            fromCreatedOn: moment().startOf("day").unix(),
            toCreatedOn: moment().endOf("day").unix(),
            Limit: self.limit,
            Offset: self.offset
          }).then(rs => {
            this.dialogVisible = true;
            this.justNow = rs;
            this.justnow_size = this.size.available;
            this.chartLoad = false;
          }).catch(e => {
            console.log(e)
          });
        },
        reload() {
          this.charts.line.destroy();
          this.chartTypeHandle(this.type);
        },
        previewAvatar(Avatar) {
          this.src = Avatar;
          this.visible = true;
        },
        justnowList() {
          this.top_pagination = this.limit > 10 && this.justnow_size > 0;
          this.dialogType = 'justnow';
          let first_logger = this.init_logger_justnow + 1;
          this.last_logger_justnow = this.staffs[0].Id + 1;
          this.title = `List of staffs checkin/out from ${this.from_time} to ${moment().format('HH:mm:ss')}`;
          let self = this;
          $.get('/dashboard/search', {
            fromId: first_logger,
            endId: self.last_logger_justnow,
            Limit: self.limit,
            Offset: self.offset
          }).then(rs => {
            this.dialogVisible = true;
            this.justNow = rs.data;
            this.justnow_size = rs.size[0].size;
          }).catch(e => {
            console.log(e)
          });
        },
        absentList() {
          this.dialogType = 'absent';
          let self = this;
          this.title = 'Absent list today'
          this.chartLoad = true;
          this.count();
          this.top_pagination = this.limit > 10 && this.justnow_size > 0;
          $.get('/dashboard/absent',
            {
              fromCreatedOn: moment().startOf('day').unix(),
              toCreatedOn: moment().endOf('day').unix(),
              limit: self.limit,
              offset: self.offset
            }).then(rs => {
            self.dialogVisible = true;
            self.justNow = rs.map(f => {
              f.AccessAt = 'Absent for now';
              f.Location = 'Absent for now';
              return f;
            });
            self.justnow_size = self.size.absent;
            self.chartLoad = false;
          }).catch(console.log)

        },
        chartTypeHandle(value, reclick = false) {
          let tmp = moment();
          switch (value) {
            case 'Minute': {
              this.chartOpt = {
                Year: tmp.format('YYYY'),
                Month: tmp.format('MM'),
                Day: tmp.format('DD'),
                Hour: tmp.format('HH')
              }
              break;
            }
            case 'Hour': {
              this.chartOpt = {
                Year: tmp.format('YYYY'),
                Month: tmp.format('MM'),
                Day: tmp.format('DD'),
              }
              break;
            }
            case 'Day': {
              this.chartOpt = {
                Year: tmp.format('YYYY'),
                Month: tmp.format('MM'),
              }
              break;
            }
            case 'Month': {
              this.chartOpt = {
                Year: tmp.format('YYYY'),
              }
              break;
            }
            case 'Year': {
              this.chartOpt = {
                Year: tmp.format('YYYY'),
              }
              break;
            }
            default: {
              value = 'Minute'
            }
          }
          this.chartOpt.Target = this.type;
          if (this.settings.areas.length > 0) {
            this.chartOpt.AreaIdList = "('" + this.settings.areas.join("', '") + "')";
          }
          let self = this;
          return new Promise((resolve, reject) => {
            $.get('/dashboard/chart', self.chartOpt).then(rs => {
              self.chartRs = rs;
              var salesChartData = {
                labels: app.chartRs.lable,
                datasets: [
                  {
                    label: 'Number of checkin/Checkout',
                    backgroundColor: 'rgba(144,226,255,0.9)',
                    borderColor: 'rgba(80,188,80,0.8)',
                    pointRadius: true,
                    pointBorderWidth: 4,
                    pointColor: '#1900ba',
                    pointStrokeColor: 'rgba(60,141,188,1)',
                    pointHighlightFill: '#63ffd4',
                    pointHighlightStroke: 'rgba(60,141,188,1)',
                    data: app.chartRs.values
                  },
                ]
              }
              var salesChartOptions = {
                maintainAspectRatio: false,
                responsive: true,
                legend: {
                  display: false
                },
                scales: {
                  xAxes: [{
                    gridLines: {
                      display: false,
                    }
                  }],
                  yAxes: [{
                    gridLines: {
                      display: false,
                    }
                  }]
                }
              }
              // This will get the first returned node in the jQuery collection.
              app.charts.line = new Chart(salesChartCanvas, {
                type: 'line',
                data: salesChartData,
                options: salesChartOptions
              })
              return resolve(rs)
            }).catch(e => {
              console.log(e);
            })
          })
        },
        count() {
          let self = this;
          let fromCreatedOn = moment().startOf('day').unix(), toCreatedOn = moment().endOf('day').unix();
          // eslint-disable-next-line no-undef
          $.get('/dashboard/api', {fromCreatedOn, toCreatedOn}).then(rs => {
            self.size.user = rs.size.user;
            self.size.staff = rs.size.staff;
            self.size.log = rs.size.log;
            self.size.absent = rs.size.absent;
            self.size.available = self.size.staff - self.size.absent;
            self.early = rs.early;
            self.late = rs.late;
            $.get('/area/list').then(r2s => {
              self.areas_list = r2s;
            }).catch(console.log)
          }).catch(e => {
            console.log(e)
          })
        },
        handleSizeChange(val) {
          this.limit = val || this.limit;
          this.top_pagination = this.limit > 10 && this.justnow_size > 0;
          let self = this;
          let params = {}, url = '';
          if (this.dialogType === 'absent') {
            url = '/dashboard/absent';
            params = {
              fromCreatedOn: moment().startOf('day').unix(),
              toCreatedOn: moment().endOf('day').unix(),
              limit: self.limit,
              offset: self.offset
            }
            this.title = 'Absent List today!'
          }
          if (this.dialogType === 'justnow') {
            url = '/dashboard/search';
            params = {
              fromId: self.init_logger_justnow,
              endId: self.last_logger_justnow,
              Limit: self.limit,
              Offset: self.offset
            };
            this.title = `List of staffs checkin/out from ${this.from_time} to ${moment().format('HH:mm:ss')}`;
          }
          if (this.dialogType === "available"){
            url = '/dashboard/available';
            params = {
              fromCreatedOn: moment().startOf('day').unix(),
              toCreatedOn: moment().endOf('day').unix(),
              limit: self.limit,
              offset: self.offset
            }
          }
          $.get(url, params).then(rs => {
            if (self.dialogType === 'justnow') {
              this.justNow = rs.data;
              this.justnow_size = rs.size[0].size;
            }
            if (self.dialogType === 'absent') {
              self.justNow = rs.map(f => {
                f.AccessAt = 'Absent for now';
                f.Location = 'Absent for now';
                return f;
              });
              self.justnow_size = self.size.absent;
              self.chartLoad = false;
            }
            if (self.dialogType === 'available') {
              this.justNow = rs;
              self.justnow_size = self.size.available;
              self.chartLoad = false;
            }
            }).catch(e => {
            console.log(e)
          });
        },
        handleCurrentChange(val) {
          this.offset = (parseInt(val) - 1) * this.limit;
          this.top_pagination = this.limit > 10 && this.justnow_size > 0;
          this.handleSizeChange();
        },
        accessList() {
          let self = this;
          $.get('/dashboard/details', {limit: self.limit}).then(stf => {
            self.init_logger_justnow = stf[0].Id || 0;
            self.staffs = stf;
          }).catch(e => {
            console.log(e);
          })
        },
        beforeClose() {
          this.justnow_size = 0;
          this.offset = 0;
          this.justNow = [];
        },
        handleID(row, column, cell, event) {
          let key = column.property;
          // let value = row[key];
          let self = this;
          if (key === 'Code' || key === 'FullName') {
            this.dialogType = "StaffLogDetail";
            this.title = `Log detail of ${row["Code"]} - ${row["FullName"]}`;
            this.top_pagination = this.limit > 10 && this.justnow_size > 0;
            if (this.settings.areas.length > 0) {
              this.chartOpt.AreaIdList = "('" + this.settings.areas.join("', '") + "')";
            }
            $.get('/dashboard/staff_details', {
              fromCreatedOn: self.settings.fromCreatedOn,
              toCreatedOn: self.settings.toCreatedOn,
              AreaIdList: self.chartOpt.AreaIdList,
              limit: self.limit,
              offset: self.offset,
              StaffId: row['Id']
            }).then(rs=>{
              self.justNow = rs.data;
              self.justnow_size = rs.size;
              self.dialogVisible = true;
              self.chartLoad = false;

            })
          }
        },
        genderChart(reclick = false) {
          let self = this;
          let query = {
            Target: 'Gender',
            CreatedOnfrom: self.settings.fromCreatedOn,
            CreatedOnto: self.settings.toCreatedOn
          }
          if (this.settings.areas.length > 0) {
            query.AreaIdList = "('" + this.settings.areas.join("', '") + "')";
          }
          // if (reclick) self.charts.pie.destroy();
          $.get('/dashboard/chart', query).then(rs => {
            let piechar = document.getElementById('pie-chart-canvas').getContext('2d');
            let pieCharData = {
              type: 'pie',
              data: {
                datasets: [
                  {
                    data: [rs.Female || 0, rs.Male || 0],
                    backgroundColor: [
                      'rgba(178,129,188,0.8)',
                      'rgba(70,133,188,0.8)',
                    ],
                    label: 'Percentage of gender'
                  }
                ],
                labels: ['Female', 'Male']
              },
              options: {
                responsive: false,
                layout: {
                  padding: {
                    left: 0,
                    right: 0,
                    top: 0,
                    bottom: 0
                  }
                }
              }
            }
            self.charts.pie = new Chart(piechar, pieCharData);
            return rs
          }).catch(e => {
            console.log(e)
          })
        },
        areaChart(reclick = false) {
          let query = {
            fromCreatedOn: this.settings.fromCreatedOn,
            toCreatedOn: this.settings.toCreatedOn
          }
          if (this.settings.areas.length > 0) {
            query.AreaIdList = "('" + this.settings.areas.join("', '") + "')";
          }
          // if (reclick) this.charts.radar.destroy();
          $.get('/dashboard/area', query).then(rs => {
            let radarChart = document.getElementById('radar-chart-canvas').getContext('2d');
            let config = {
              type: rs[0].length > 2 ? 'radar' : 'bar',
              data: {
                labels: rs[0],
                datasets: [
                  {
                    label: 'Male',
                    backgroundColor: 'rgba(70,133,188,0.2)',
                    borderColor: 'rgba(25,0,186,0.8)',
                    pointBackgroundColor: 'rgba(128,131,186,0.8)',
                    data: rs[1]
                  },
                  {
                    label: 'Female',
                    backgroundColor: 'rgba(249,199,221,0.2)',
                    borderColor: 'rgba(249,76,114,0.8)',
                    pointBackgroundColor: 'rgba(249,76,114,0.8)',
                    data: rs[2]
                  }
                ]
              },
              options: {
                responsive: false,
                maintainAspectRatio: false,
                layout: {
                  padding: {
                    left: 0
                  }
                },
                title: {
                  display: true,
                  text: 'Ratio of gender in different area'
                },
                // scale: {
                //   ticks: {
                //     beginAtZero: true
                //   }
                // }
              }
            }
            app.charts.radar = new Chart(radarChart, config);
          }).catch(e => {
            console.log(e);
          })
        },
        mountList(opt, reclick=false) {
          this.settings.fromCreatedOn = convertDate(this.dateRange[0]);
          this.settings.toCreatedOn = convertDate(this.dateRange[1]);
          this.charts = {};
          this.count();
          this.accessList();
          this.chartTypeHandle(opt, reclick);
          this.genderChart(reclick);
          this.areaChart(reclick);
        },
        tableRowClassName(line) {
          let row = line.row;
          if (row.CheckIn) {
            return 'checkin';
          } else {
            return 'checkout';
          }
        },
      },
      mounted() {
        this.mountList('Day', false);
      }
    });
    var salesChartCanvas = document.getElementById('revenue-chart-canvas').getContext('2d');
    //$('#revenue-chart').get(0).getContext('2d');
    // eslint-disable-next-line no-undef
    socket.on('user_size', function () {
      app.size.user += 1;
    });
    // eslint-disable-next-line no-undef
    socket.on('staff_size', function () {
      app.size.staff += 1;
    });

    // eslint-disable-next-line no-undef
    socket.on('log_size', function () {
      app.size.log += 1;
      app.size.just += 1;
    });

    // let table = document.getElementById()
    // eslint-disable-next-line no-undef
    socket.on('log', function (msg) {
      msg.justnow = true;
      app.staffs.unshift(msg);
      if (app.staffs.length > app.limit) app.staffs.pop();
    })
  })

})