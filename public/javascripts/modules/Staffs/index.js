$(document).ready(f => {
  let app = new Vue({
    el: '#app',
    data() {
      return {
        visible: false,
        src: '',
        model: {
          Code: '',
          FullName: '',
          Dob: '',
          Location: '',
          Avatar: '',
          MobileNumber: ''
        },
        rules: {
          Code: [
            {required: true, message: "Please fill username", trigger: ['blur', 'change']},
            {min: 3, max: 20, message: 'Length should be 3 to 20', trigger: ['blur', 'change']}
          ],
          FullName: [
            {required: true, message: "Please fill first name", trigger: ['blur', 'change']},
            {min: 3, max: 20, message: 'Length should be 3 to 20', trigger: ['blur', 'change']}
          ],
          Location: [
            {required: true, message: "Please fill last name", trigger: ['blur', 'change']},
            {min: 3, max: 250, message: 'Length should be 3 to 20', trigger: ['blur', 'change']}
          ],
          MobileNumber: [
            {required: true, message: "Please fill mobile number", trigger: ['blur', 'change']},
            {min: 5, max: 11, message: 'Length should be 5 to 11', trigger: ['blur', 'change']}
          ]
        },
        models: [],
        loading: {
          create: false,
          delete: false,
          search: false,
          list: false
        },
        search: '',
        limit: 10,
        offset: 0,
        activeName: 'list',
        size: 0,
        dialogImageUrl: '',
        dialogVisible: false,
        disabled: false
      };
    },
    methods: {
      onSubmit(formName) {
        this.trimAll(this.model);
        let self = this;
        self.model.Dob = new Date(self.model.Dob).toLocaleDateString();
        this.$refs[formName].validate((valid) => {
          if (valid) {
            self.loading.create = true;
            $.post('/staffs/add', {staff: self.model}).then(rs => {
              self.loading.create = false;
              if (rs.status) {
                self.$notify({
                  type: "success",
                  title: "Staff Created",
                  message: "Successfully create Staff!"
                })
                self.$refs[formName].resetFields();
                self.activeName = 'list';
              } else {
                self.$notify({
                  type: "error",
                  title: "Staff Created",
                  message: rs.message
                })
              }
            }).catch(e => {
              console.log(e)
              self.loading.create = false;
              self.$notify({
                type: "error",
                title: "Staff Creation!",
                message: e.sqlMessage
              })
            })
          } else {
            this.$notify({
              type: "warning",
              title: "Staff validation!",
              message: "Please check staff validation!"
            })
            return false;
          }
        });
      },
      list() {
        let self = this;
        this.loading.list = true;
        $.get('/staffs/list', {limit: self.limit, offset: self.offset, search: self.search}, data => {
          self.models = data.data;
          this.loading.list = false;
          self.size = data.size;
        })
      },
      search_user() {
        let self = this;
        this.loading.search = true;
        $.get('/staffs/search', {limit: self.limit, offset: self.offset, search: self.search}).then(data => {
          self.models = data.data.map(d => {
            d.FullName = d.FirstName + ' ' + d.LastName;
            d.status = d.Status === 1 ? "Active" : "Inactive";
            return d;
          });
          self.size = data.size;
          self.loading.search = false;
        }).catch(e => {
          console.log(e);
        })
      },
      handleClick(line) {
        console.log(line.Id)
        // let row = line.row;
        // console.log(row.Id);
      },
      userAdd() {
        this.activeName = "create"
      },
      trimAll(object) {
        for (let k in object) {
          if (k === 'Dob') continue;
          object[k] = object[k].trim();
        }
      },
      handleSizeChange(val) {
        this.limit = val;
        if (this.search) {
          this.search_user()
        } else {
          this.list()
        }
      },
      handleCurrentChange(val) {
        this.offset = (parseInt(val) - 1) * this.limit;
        if (this.search) {
          this.search_user()
        } else {
          this.list()
        }
      },
      handleRemove(file) {
        console.log(file);
      },
      handlePictureCardPreview(file) {
        this.dialogImageUrl = file.url;
        this.dialogVisible = true;
      },
      handleDownload(file) {
        console.log(file);
      },
      previewAvatar(url, title){
        this.src = url;
        this.title = `Avatar of ${title}`;
        this.visible = true;
      }
    },
    mounted() {
      this.list();
    }
  });

  socket.on('staff', staff => {
    if (app.search.length === 0) {
      if (app.offset == 0) {
        app.models.unshift(staff);
        if(app.models.length > app.limit) app.models.pop();
      }
      app.size += 1;
    }
  })
})
