$(document).ready(f => {
  let app = new Vue({
    el: '#app',
    data() {
      return {
        model: {
          UserName: '',
          FirstName: '',
          LastName: '',
          Email: '',
          Dob: '',
          MobileNumber: ''
        },
        rules: {
          UserName: [
            {required: true, message: "Please fill username", trigger: ['blur', 'change']},
            {min: 3, max: 20, message: 'Length should be 3 to 20', trigger: ['blur', 'change']}
          ],
          FirstName: [
            {required: true, message: "Please fill first name", trigger: ['blur', 'change']},
            {min: 3, max: 20, message: 'Length should be 3 to 20', trigger: ['blur', 'change']}
          ],
          LastName: [
            {required: true, message: "Please fill last name", trigger: ['blur', 'change']},
            {min: 3, max: 20, message: 'Length should be 3 to 20', trigger: ['blur', 'change']}
          ],
          Email: [
            {required: true, message: "Please fill email", trigger: ['blur', 'change']},
            {min: 10, max: 120, message: 'Length should be 10 to 120', trigger: ['blur', 'change']},
            {type: 'email', message: 'Email invalid', trigger: ['blur', 'change']}
          ],
          MobileNumber: [
            {required: true, message: "Please fill mobile number", trigger: ['blur', 'change']},
            {min: 5, max: 11, message: 'Length should be 5 to 11', trigger: ['blur', 'change']}
          ]
        },
        models: [],
        loading: {
          create: false,
          delete: false,
          search: false,
          list: false
        },
        size: 0,
        search: '',
        limit: 10,
        offset: 0,
        activeName: 'list'
      };
    },
    methods: {
      onSubmit(formName) {
        this.trimAll(this.model);
        let self = this;
        this.$refs[formName].validate((valid) => {
          if (valid) {
            self.loading.create = true;
            $.post('/users/add', {user: self.model}).then(rs=>{
              self.loading.create = false;
              self.$notify({
                type: "success",
                title:"User Created",
                message: "Successfully create user!"
              })
              this.$refs[formName].resetFields();
              this.activeName='list'
            }).catch(e=>{
              self.loading.create = false;
              self.$notify({
                type: "error",
                title:"User Creation!",
                message: e.sqlMessage
              })
            })
          } else {
            this.$notify({
              type: "warning",
              title:"User validation!",
              message: "Please check user validation!"
            });
            return false;
          }
        });
      },
      list() {
        let self = this;
        this.loading.list = true;
        $.get('/users/list', {limit: self.limit, offset: self.offset, search: self.search}, data => {
          self.models = data.data.map(d => {
            d.FullName = d.FirstName + ' ' + d.LastName;
            d.status = d.Status === 1 ? "Active" : "Inactive";
            d.loading = false;
            return d;
          });
          this.loading.list = false;
          self.size = data.size;
        })
      },
      search_user() {
        let self = this;
        this.loading.search = true;
        $.get('/users/search', {limit: self.limit, offset: self.offset, search: self.search}).then(data => {
          self.models = data.data.map(d => {
            d.FullName = d.FirstName + ' ' + d.LastName;
            d.status = d.Status === 1 ? "Active" : "Inactive";
            return d;
          });
          self.size = data.size;
          self.loading.search = false;
        }).catch(e => {
          console.log(e);
        })
      },
      tableRowClassName(line) {
        let row = line.row;
        if (row.Status === 1) {
          return 'success-row';
        }
        if (row.Status === 0) return 'error-row';
      },
      handleClick(line) {
        console.log(line.Id)
      },
      userAdd() {
        this.activeName = "create"
      },
      handleSwitch(index) {
        let line = this.models[index];
        line.loading = true;
        let status = line.Status === 1 ? 0 : 1;
        $.post('/users/toggle', {id: line.Id, status: line.Status === 1 ? 0 : 1}).then(rs => {
          if (rs.status === 1) {
            line.Status = status;
            line.status = line.Status === 1 ? 'Active' : 'Inactive'
          }
          line.loading = false;
        }).catch(e => {
          console.log(e)
        })
      },
      trimAll(object){
        for (let k in object){
          if (k === 'Dob') continue;
          object[k] = object[k].trim();
        }
      },
      handleSizeChange(val){
        this.limit = val;
        if (this.search){
          this.search_user()
        }else {
          this.list()
        }
      },
      handleCurrentChange(val){
        this.offset = (parseInt(val) - 1) * this.limit;
        if (this.search){
          this.search_user()
        }else {
          this.list()
        }
      }
    },
    mounted() {
      this.list();
    }
  });
})
