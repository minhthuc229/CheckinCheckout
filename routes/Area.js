let express = require('express');
let router = express.Router();
const Gateway = require('../controller/Gateway');
let gateway = new Gateway();

/* GET Users listing. */
router.get('', (req, res, next)=>{
  gateway.area.index(req, res, next)
});
router.get('/list', (req, res, next)=>{
  gateway.area.list(req ,res, next);
});

module.exports = router;
