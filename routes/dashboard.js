let express = require('express');
let router = express.Router();
const Gateway = require('../controller/Gateway');
let gateway = new Gateway();

/* GET Users listing. */
router.get('', (req, res, next)=>{
  gateway.report.index(req, res, next)
})
router.get('/api', (req, res, next)=>{
  gateway.report.api(req ,res, next);
});

router.get('/details', (req, res, next)=>{
  gateway.report.logList(req, res, next);
});

router.get('/chart', (req, res, next)=>{
  gateway.report.logStatistic(req, res, next);
});

router.get('/search', (req, res, next)=>{
  gateway.report.search(req, res, next);
});

router.get('/area', (req, res, next)=>{
  gateway.report.listArea(req, res, next);
});

router.get('/absent', (req, res, next)=>{
  gateway.report.absentList(req, res, next);
});

router.get('/available', (req, res, next)=>{
  gateway.report.availableList(req, res, next);
});

router.get('/checkin', (req, res, next)=>{
  gateway.report.checkinIndex(req, res, next)
});

router.get('/staff_details', (req, res, next)=>{
  gateway.report.StaffDetail(req, res, next)
});


module.exports = router;
