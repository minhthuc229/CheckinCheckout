function index(gateway) {
  this.routers = [];
  this.routers.push({
    url: '', controller: gateway.staticPage.login_index, method: 1
  });
  this.routers.push({url: 'login', controller: gateway.staticPage.login, method: 1});
  this.routers.push({
    url: 'logout', controller: gateway.staticPage.logout, method: 1
  });
}

module.exports = index;