let express = require('express');
let router = express.Router();
const Gateway = require('../controller/Gateway');
const passport = require('passport'), auth = require('../helper/auth');
let Index = require('./index');
let gateway = new Gateway();

class RouterBuilder{
  constructor(){
    this.GET = 1;
    this.POST = 2;
    this.DELETE = 4;
    this.router = router;
    this.gateway = gateway;
    this.router.post('/login', (req, res, next)=>{
      passport.authenticate('local', function(err, user) {
        if (err) { return next(err); }
        if (!user) { return res.send(false) }
        let token = auth.sign({Id: user.Id}, gateway.PRIVATEKEY, 3600);
        gateway.db.user.upsert(user.Id, {AccessToken: token}).then(()=>{
          user.AccessToken = token;
          req.logIn(user, function(err) {
            if (err) { return next(err); }
            return res.send(user);
          });
        }).catch(e=>{
          console.log(e);
          return res.send(false);
        });
      })(req, res, next);
    });
    this.gateway.init().then(()=>{
      this.use('', new Index(this.gateway).routers);
    })
  }

  /*
  * prefix, router
  * router = [
  *   {url: <string>, controller: function(req, res, next), method: <number>}
  * ]
  * */
  use(prefix, routers){
    prefix = '/' + prefix;
    for (let i = 0 ; i < routers.length; i++){
      let route = routers[i];
      console.log(route.controller)
      if (route.method === this.GET){
        this.router.get(prefix, (req, res, next)=>{
          route.controller(req, res, next)
        })
      }
      if (route.method === this.POST){
        this.router.post(prefix + route.url, (req, res, next)=>{
          route.controller(req, res, next)
        })
      }

      if (route.method === this.DELETE){
        this.router.delete(prefix + route.url, (req, res, next)=>{
          route.controller(req, res, next)
        })
      }
    }
  }

}

module.exports = new RouterBuilder;
