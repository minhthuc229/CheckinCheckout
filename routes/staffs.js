let express = require('express');
let router = express.Router();
const Gateway = require('../controller/Gateway');
let gateway = new Gateway();

/* GET Users listing. */
router.get('', (req, res, next)=>{
  gateway.staff.index(req, res, next)
});
router.get('/list', function (req, res, next) {
  gateway.staff.list(req, res, next);
});
router.get('/search', (req, res, next)=>{
  gateway.user.user_search(req, res, next);
})

router.get('/count', (req, res, next)=>{
  gateway.staff.countAll(req,res, next);
})

router.post('/add', (req, res, next)=>{
  gateway.staff.addStaff(req, res, next);
})


module.exports = router;
