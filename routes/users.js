let express = require('express');
let router = express.Router();
const Gateway = require('../controller/Gateway');
let gateway = new Gateway();

/* GET Users listing. */
router.get('', (req, res, next)=>{
  gateway.user.index(req, res, next)
})
router.get('/list', function (req, res, next) {
  gateway.user.list(req, res, next);
});
router.get('/search', (req, res, next)=>{
  gateway.user.user_search(req, res, next);
})

router.post('/toggle', (req, res, next)=>{
  gateway.user.toggleStatus(req, res, next);
})

router.post('/add', (req, res, next)=>{
  gateway.user.addUser(req, res, next);
})

router.get('/count', (req, res, next)=>{
  gateway.user.countAll(req, res, next);
})

module.exports = router;
