let faker = require('faker');
const auth = require('../helper/auth');
const helper = require('../helper/Sql');
faker.locale = 'en';
const moment = require('moment');
let model = require('../model/Gateway/InitialConnect');
let config = require('../config/dev');
let m = new model(config);

function format(date) {
  let FormatDate = moment(date, 'YYYY-MM-DDTHH:mm:ss');
  return {
    Year: parseInt(FormatDate.format('YYYY')),
    Month: parseInt(FormatDate.format('MM')),
    Day: parseInt(FormatDate.format('DD')),
    Hour: parseInt(FormatDate.format('HH')),
    Minute: parseInt(FormatDate.format('mm')),
    Second: parseInt(FormatDate.format('ss')),
  }
}

async function seed(number) {
  for (let i = 0; i < number; i++) {
      let area = {
        Name: faker.company.companyName(),
        CreatedOn: moment().unix(),
      }
      try {
        let rs = await m.area.insert(area);
        console.log("area inserted")
        if (i == number -1){
          process.exit()
        }
      }catch (e) {
        if (i == number -1){
          process.exit()
        }
        console.log(e);
      }
  }
  process.exit()

}

seed(10)
