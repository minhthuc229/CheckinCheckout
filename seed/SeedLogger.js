let faker = require('faker');
const auth = require('../helper/auth');
const helper = require('../helper/Sql');
faker.locale = 'en';
const moment = require('moment');
let model = require('../model/Gateway/InitialConnect');
let config = require('../config/dev');
let m = new model(config);

function format(date) {
  let FormatDate = moment(date, 'YYYY-MM-DDTHH:mm:ss');
  return {
    Year: parseInt(FormatDate.format('YYYY')),
    Month: parseInt(FormatDate.format('MM')),
    Day: parseInt(FormatDate.format('DD')),
    Hour: parseInt(FormatDate.format('HH')),
    Minute: parseInt(FormatDate.format('mm')),
    Second: parseInt(FormatDate.format('ss')),
  }
}

async function seed(number) {
  let max = await m.staff.countAll();
  let max_area = await m.area.countAll();
  for (let i = 0; i < number; i++) {
    setTimeout(function () {
      let staffID = faker.random.number({min: 1, max});
      let AreaId = faker.random.number({min: 1, max: max_area});

      let staff = m.staff.get(staffID);
      let date = format(faker.date.between('2019-01-01', '2019-12-31'));
      let log = {
        StaffId: staffID,
        Year: date.Year,
        Month: date.Month,
        Day: date.Day,
        Hour: date.Hour,
        Minute: date.Minute,
        Second: date.Second,
        CreatedOn: moment(helper.objectTimeToString(date), 'DD/MM/YYYY HH:mm:ss').unix(),
        CheckIn: i % 2 === 1,
        AreaId: AreaId
      }
      let rs = m.log.insert(log);
      Promise.all([staff, rs]).then(result => {
        let stf = result[0];
        log.Code = stf.Code;
        log.FullName = stf.FullName;
        log.Location = stf.Location;
        log.AccessAt = helper.objectTimeToString(log);
        log.Id = result[1];
        log.Avatar = stf.Avatar;
        m.log.notify('log_size', rs);
        m.log.notify('log', log);
        console.log("log inserted", i)
      }).catch(e => {
        console.log(e)
      })
      if (i == number -1){
        process.exit()
      }
    }, 10 * i)

  }
}

seed(1000)