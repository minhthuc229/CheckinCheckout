let faker = require('faker');
const auth = require('../helper/auth');
faker.locale = 'en';
const moment = require('moment');
let model = require('../model/Gateway/InitialConnect');
let config = require('../config/dev');
let m = new model(config);
async function seed(number) {
  /**
   *
   * @type {{MobileNumber: *, UserName: string, Email: string, FirstName: string, Dob: string, AccessToken: *, LastName: string, PassWord: string}}
   * belong to User
   */
  let admin = {
    UserName: "admin",
    LastName: "admin",
    FirstName: "admin",
    Dob: "1995-02-02",
    PassWord: "123456789",
    Email: "admin@gmail.com",
    MobileNumber: faker.phone.phoneNumber(),
  }
  try{
    m.user.insert(admin).then(rs=>{
      console.log(rs)
    }).catch(e=>{
      console.log(e)
    });

  }catch (e) {
    console.log(e)
  }
  for (let i = 0 ; i < number; i++){
    try{
      setTimeout(async ()=>{
        let staff = {
          Code: auth.saltGeneration(3).toUpperCase(),
          FullName: faker.name.findName(),
          Dob: moment(faker.date.between('1990-01-01', '2000-12-31'), 'YYYY-MM-DD').unix(),
          Location: faker.address.streetAddress(),
          Avatar: faker.image.avatar(),
          Gender: faker.random.arrayElement(["Male", "Female"]),
        };
        let id = await m.staff.insert(staff);
        console.log("ok");
        staff.Id = id.data;
        staff.Dob = moment.unix(staff.Dob).format('DD/MM/YYYY')
        staff.CreatedOn = moment.unix(staff.CreatedOn).format('DD/MM/YYYY')
        m.staff.notify('staff', staff);
        if (i == number -1){
          process.exit()
        }
      }, 5 * i)
    }catch (e) {
      console.log(e)
    }
  }

  /**
   * Belong to Staff        Checkin: faker.random.boolean()

   */



}
seed(100)