let faker = require('faker');
const auth = require('../helper/auth');
const helper = require('../helper/Sql');
faker.locale = 'en';
const moment = require('moment');
let model = require('../model/Gateway/InitialConnect');
let config = require('../config/dev');
let m = new model(config);

function format(date) {
  let FormatDate = moment(date, 'YYYY-MM-DDTHH:mm:ss');
  return {
    Year: parseInt(FormatDate.format('YYYY')),
    Month: parseInt(FormatDate.format('MM')),
    Day: parseInt(FormatDate.format('DD')),
    Hour: parseInt(FormatDate.format('HH')),
    Minute: parseInt(FormatDate.format('mm')),
    Second: parseInt(FormatDate.format('ss')),
  }
}

async function seed(number) {
  let max = await m.area.countAll();
  for (let i = 0; i < number; i++) {
    setTimeout(async function () {
      let date = format(faker.date.between('2019-01-01', '2019-12-31'));
      let areaID = faker.random.number({min: 1, max});
      let stranger = {
        AreaId: areaID,
        Year: date.Year,
        Month: date.Month,
        Day: date.Day,
        Hour: date.Hour,
        Minute: date.Minute,
        Second: date.Second,
        Images: faker.image.avatar(),
        CreatedOn: moment().unix(),
        Checkin: faker.random.boolean()
      }
      let rs = await m.stranger.insert(stranger);
      console.log("ok");
      if (i == number -1){
        process.exit()
      }
    }, 50 * i)

  }
}

seed(100)