var assert = require('assert'), config = require('../config/dev'), db = require('../model/Gateway/InitialConnect');
let chai = require('chai');
let example = require('./FactoryGirl'), moment = require('moment');
let expect = chai.expect;

function fastGenText(number, postfix = '') {
  let string = '';
  for (let i = 0; i < number; i++) {
    string += 'a';
  }
  return string + postfix;
}

describe('Area Connection', function () {
  let connection = new db(config);
  let area = example.area;
  after(() => {
    connection.area.query(`Delete from Area where Name = '${area.Name}'`);
  })
  describe('Create Area', () => {

    describe('should be unsuccessful', function () {

      it('Name length is overlength 101', function (done) {
        connection.area.insert({...area, Name: fastGenText(101)}).then(rs => {
          done('fails')
        }).catch(e => {
          done();
        })
      });

      it('Name length is underlength 5', function (done) {
        connection.area.insert({...area, Name: fastGenText(4)}).then(rs => {
          console.log(rs)
          done('fails')
        }).catch(e => {
          done();
        })
      });

      it('Name length is null', function (done) {
        connection.area.insert({...area, Name: null}).then(rs => {
          done('fails')
        }).catch(e => {
          done();
        })
      });
    });

    describe('Area Exist', () => {
      let count = 0;
      before(async () => {
        count = await connection.area.countAll();
      });

      it('should increase by 1 when create an area successfully', function (done) {
        connection.area.insert(area).then(() => {
          connection.area.countAll().then(size => {
            if (size === count + 1) {
              done();
            } else {
              done('false');
            }
          }).catch(e2 => {
            done(e2);
          })
        }).catch(e => {
          done(e);
        });
      });

      it('should include area at first', function (done) {
        connection.area.list().then(rs => {
          let exist = rs.find(r => r.Name === area.Name);
          if (exist && rs[0].Name === area.Name) {
            done()
          } else {
            done('fails');
          }
        })
      });

    })

  });

});