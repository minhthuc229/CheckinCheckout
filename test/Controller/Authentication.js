let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../../app');
let should = chai.should(), expect = require('chai').expect;
chai.use(chaiHttp);
describe('/Post Login', () => {
  it('should log in successfully', function (done) {
    chai.request(server).post('/login').send({
      UserName: 'admin',
      password: '123456789',
    }).end((err, res) => {
      res.body.should.be.eql(true);
      res.should.have.status(200);
      done()
    })
  });

  it('should log in unsuccessfully when login with incorrect account', function (done) {
    chai.request(server).post('/login').send({
      UserName: 'admin',
      password: '12345629',
    }).end((err, res) => {
      res.body.should.be.eql(false);
      res.should.have.status(200);
      done()
    })
  });
})