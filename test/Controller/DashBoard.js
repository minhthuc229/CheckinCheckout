let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../../app');
let should = chai.should(), expect = require('chai').expect;
chai.use(chaiHttp);
describe('DashBoard', () => {
  describe('without Login', () => {
    it('GET /dashboard should redirect to Login page', function (done) {
      chai.request(server).get('/dashboard').end((err, res) => {
        expect(res.text.includes('<title>login')).to.equal(true);
        done();
      })
    });

    it('GET /dashboard/api should return {}', function (done) {
      chai.request(server).get('/dashboard/api').end((err, res) => {
        expect(res.body).to.eql({});
        done();
      })
    });

    it('GET /dashboard/chart should return {}', function (done) {
      chai.request(server).get('/dashboard/chart').end((err, res) => {
        expect(res.body).to.eql({});
        done();
      })
    });

    it('GET /dashboard/search should return {}', function (done) {
      chai.request(server).get('/dashboard/search').end((err, res) => {
        expect(res.body).to.eql([]);
        done();
      })
    });

    it('GET /dashboard/area should return {}', function (done) {
      chai.request(server).get('/dashboard/area').end((err, res) => {
        expect(res.body).to.eql({});
        done();
      })
    });

    it('GET /dashboard/absent should return {}', function (done) {
      chai.request(server).get('/dashboard/absent').end((err, res) => {
        expect(res.body).to.eql([]);
        done();
      })
    });

    it('GET /dashboard/checkin should return {}', function (done) {
      chai.request(server).get('/dashboard/checkin').end((err, res) => {
        res.should.have.status(404);
        done();
      })
    });

    it('GET /dashboard/staff_details should return {}', function (done) {
      chai.request(server).get('/dashboard/staff_details').end((err, res) => {
        expect(res.body).to.eql([]);
        done();
      })
    });
  })
  let Cookies;
  describe('Login function', () => {
    it('should create user session for valid user', function (done) {
      chai.request(server)
        .post('/login')
        .send({"UserName": "admin", "password": "123456789"})
        .end(function (err, res) {
          Cookies = res.headers['set-cookie'].pop().split(';')[0];
          done();
        });
    });
    it('Get /dashboard/api should return status 501 without params', function (done) {
      let req = chai.request(server).get('/dashboard/api');
      req.cookies = Cookies;
      req.end((err, res) => {
        res.should.have.status(501);
        done()
      })
    });

    it('GET /dashboard/api should return 200 and meaningful result', function (done) {
      let req = chai.request(server).get('/dashboard/api');
      req.cookies = Cookies;
      req.query({
        fromCreatedOn: 1569776400,
        toCreatedOn: 1569862799
      }).end((err, res) => {
        expect(res.body).to.have.all.keys('early', 'late', 'size');
        expect(res.body.size).to.have.all.keys('absent', 'log', 'staff', 'user');
        done()
      })
    });

    it('GET /dashboard/details without params', function (done) {
      let req = chai.request(server).get('/dashboard/details');
      req.cookies = Cookies;
      req.end((err, res) => {
        expect(res.body).to.have.lengthOf(10);
        done()
      })
    });

    it('GET /dashboard/details with limit over 100', function (done) {
      let req = chai.request(server).get('/dashboard/details');
      req.cookies = Cookies;
      req.query({limit: 101}).end((err, res) => {
        expect(res.body).to.have.lengthOf(0);
        done()
      })
    });

    it('GET /dashboard/details with limit 20', function (done) {
      let req = chai.request(server).get('/dashboard/details');
      req.cookies = Cookies;
      req.query({limit: 20}).end((err, res) => {
        expect(res.body).to.have.lengthOf(20);
        done()
      })
    });

    it('GET /dashboard/chart return {} without Target', function (done) {
      let req = chai.request(server).get('/dashboard/chart');
      req.cookies = Cookies;
      req.query({Year: 2019}).end((err, res) => {
        res.body.should.to.eql({});
        done()
      })
    });

    it('GET /dashboard/chart return {} with only Target', function (done) {
      let req = chai.request(server).get('/dashboard/chart');
      req.cookies = Cookies;
      req.query({Target: "Gender"}).end((err, res) => {
        res.body.should.to.eql({});
        done()
      })
    });

    it('GET /dashboard/chart should return {} without any params', function (done) {
      let req = chai.request(server).get('/dashboard/chart');
      req.cookies = Cookies;
      req.end((err, res) => {
        res.body.should.to.eql({});
        done()
      })
    });

    it('GET /dashboard/chart should return {} with wrong target', function (done) {
      let req = chai.request(server).get('/dashboard/chart');
      req.cookies = Cookies;
      req.query({
        Target: "Just",
        Year: 2019,
        Month: 9
      }).end((err, res) => {
        res.body.should.to.eql({});
        done()
      })
    });

    it('GET /dashboard/chart should return all day of month with Day Target', function (done) {
      let req = chai.request(server).get('/dashboard/chart');
      req.cookies = Cookies;
      req.query({
        Target: "Day",
        Year: 2019,
        Month: 9
      }).end((err, res) => {
        expect(res.body).to.have.all.keys('lable', 'values');
        expect(res.body.lable).to.have.lengthOf(30);
        expect(res.body.values).to.have.lengthOf(30);
        done()
      })
    });

    it('GET /dashboard/chart should return all Hour of day with Hour Target', function (done) {
      let req = chai.request(server).get('/dashboard/chart');
      req.cookies = Cookies;
      req.query({
        Target: "Hour",
        Year: 2019,
        Month: 9,
        Day: 10
      }).end((err, res) => {
        expect(res.body).to.have.all.keys('lable', 'values');
        expect(res.body.lable).to.have.lengthOf(24);
        expect(res.body.values).to.have.lengthOf(24);
        done()
      })
    });

    it('GET /dashboard/chart should return all Minute of Hour with Minute Target', function (done) {
      let req = chai.request(server).get('/dashboard/chart');
      req.cookies = Cookies;
      req.query({
        Target: "Minute",
        Year: 2019,
        Month: 9,
        Day: 10,
        Hour: 23
      }).end((err, res) => {
        expect(res.body).to.have.all.keys('lable', 'values');
        expect(res.body.lable).to.have.lengthOf(60);
        expect(res.body.values).to.have.lengthOf(60);
        done()
      })
    });

    it('GET /dashboard/chart should return all 0 values of Hour with Minute Target and invalid condition', function (done) {
      let req = chai.request(server).get('/dashboard/chart');
      req.cookies = Cookies;
      req.query({
        Target: "Minute",
        Year: 2019,
        Month: 9,
        Day: 10,
        Hour: 24
      }).end((err, res) => {
        expect(res.body).to.have.all.keys('lable', 'values');
        expect(res.body.lable).to.have.lengthOf(60);
        expect(res.body.values).to.have.lengthOf(60);
        for (let i = 0; i < res.body.values.length; i++){
          res.body.values[i].should.to.equal(0);
        }
        done()
      })
    });
  })
})