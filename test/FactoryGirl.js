const moment = require('moment');

module.exports = {
  user: {
    UserName: 'randomtest',
    PassWord: "123456789",
    Dob: moment().format('YYYY-MM-DD'),
    FirstName: 'Minhthuc',
    LastName: 'Truong',
    salt: 'zdascaswdas',
    MobileNumber: '1231244566',
    Email: "minhthuc225@gmail.com",
    AccessToken: "askjdhowidklasjiw"
  },
  staff: {
    Code: 'zzzzzzz',
    FullName: 'Normal Staff',
    MobileNumber: '1231291248',
    Location: 'Just a location test',
    Avatar: 'just avatatest',
    Gender: 'Famale',
    BornIn: 1995
  },
  area: {
    Name: 'Area10'
  },
  stranger: {
    AreaId: 38,
    Year: 2019,
    Month: 10,
    Day: 18,
    Hour: 6,
    Minute: 22,
    Second: 32,
    Images: 'https://s3.amazonaws.com/uifaces/faces/twitter/hermanobrother/128.jpg',
    CreatedOn: 1569577372,
    Checkin: true
  }
}