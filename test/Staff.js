var assert = require('assert'), config = require('../config/dev'), db = require('../model/Gateway/InitialConnect');
let chai = require('chai');
let example = require('./FactoryGirl'), moment = require('moment');
let expect = chai.expect;

function fastGenText(number, postfix = '') {
  let string = '';
  for (let i = 0; i < number; i++) {
    string += 'a';
  }
  return string + postfix;
}

describe('Staff Connection', function () {
  let connection = new db(config);
  let staff = example.staff;
  after(() => {
    connection.staff.query(`Delete from Staff where Code = '${staff.Code}'`);
  })
  describe('Create Staff', () => {

    describe('should be unsuccessful', function () {

      it('Code length is overlength 21', function (done) {
        connection.staff.insert({...staff, Code: fastGenText(21)}).then(rs => {
          done('fails')
        }).catch(e => {
          done();
        })
      });

      it('Code length is underlength 5', function (done) {
        connection.staff.insert({...staff, Code: fastGenText(4)}).then(rs => {
          done('fails')
        }).catch(e => {
          done();
        })
      });

      it('Code length is null', function (done) {
        connection.staff.insert({...staff, Code: null}).then(rs => {
          done('fails')
        }).catch(e => {
          done();
        })
      });

      it('FullName length is overlength 21', function (done) {
        connection.staff.insert({...staff, FullName: fastGenText(21)}).then(rs => {
          done('fails')
        }).catch(e => {
          done();
        })
      });

      it('FullName length is underlength 4', function (done) {
        connection.staff.insert({...staff, FullName: fastGenText(4)}).then(rs => {
          done('fails')
        }).catch(e => {
          done();
        })
      })

      it('FullName length is null', function (done) {
        connection.staff.insert({...staff, FullName: null}).then(rs => {
          done('fails')
        }).catch(e => {
          done();
        })
      });

      it('Dob length is not number', function (done) {
        connection.staff.insert({...staff, Dob: fastGenText(21)}).then(rs => {
          done('fails')
        }).catch(e => {
          done();
        })
      });

      it('MobileNumber length is 31', function (done) {
        connection.staff.insert({...staff, MobileNumber: fastGenText(31)}).then(rs => {
          done('fails')
        }).catch(e => {
          done();
        })
      });

    });

    describe('Staff Exist', () => {
      let second_staff = {
        Code: 'zzzzzzza',
        FullName: 'Normal Staff 2',
        MobileNumber: '1231291248',
        Location: 'Just a location test',
        Avatar: 'just avatatest',
        Gender: 'Famale',
        BornIn: 1995
      }
      let count = 0;
      before(async () => {
        count = await connection.staff.countAll();
      });

      it('should increase by 1 when create a staff successfully', function (done) {
        connection.staff.insert(staff).then(() => {
          connection.staff.countAll().then(size => {
            if (size === count + 1) {
              done();
            } else {
              done('false');
            }
          }).catch(e2 => {
            done(e2);
          })
        }).catch(e => {
          done(e);
        });
      });

      it('should not be successful when duplicate a staff', done => {
        connection.staff.insert(staff).then(rs => {
          if (rs.code === 0){
            done()
          } else {
            done('fails')
          }
        }).catch(e => {
          console.log(e)
          done()
        })
      });

      it('should not be successful when Code exist', done => {
        connection.staff.insert({...second_staff, Code: staff.Code}).then(rs => {
          if(rs.code === 1) done('staff still inserted');
          if(rs.code === 0) done();
        }).catch(e => {
          done()
        })
      });

      it('should include staff at first', function (done) {
        connection.staff.listStaffs(10, 0).then(rs => {
          let exist = rs.data.find(r => r.Code === staff.Code);
          if (exist && rs.data[0].Code === staff.Code) {
            done()
          } else {
            done('fails');
          }
        })
      });

      it('should have exactly count', function (done) {
        connection.staff.listStaffs(10, 0).then(rs => {
          if (rs.size === (count + 1)) {
            done();
          }else{
            done("false");
          }
        })
      });

      it('should be find staff successfully', function (done) {
        connection.staff.findStaffCode(staff.Code).then(u=>{
          if (u) return done()
          done('fails')
        })
      });

    })

  });

});