var assert = require('assert'), config = require('../config/dev'), db = require('../model/Gateway/InitialConnect');
let chai = require('chai');
let example = require('./FactoryGirl'), moment = require('moment');
let expect = chai.expect;
const helper = require('../helper/Sql');

function fastGenText(number, postfix = '') {
  let string = '';
  for (let i = 0; i < number; i++) {
    string += 'a';
  }
  return string + postfix;
}

describe('Stranger Connection', function () {
  let connection = new db(config);
  let stranger = example.stranger;
  after(() => {
    connection.stranger.query(`Delete from Stranger where ${helper.ObjectToWhere(stranger)}`);
  })
  describe('Create Stranger', () => {

    describe('should be unsuccessful', function () {

      it('Year is not a number', function (done) {
        connection.stranger.insert({...stranger, Year: fastGenText(101)}).then(rs => {
          if (!rs.status) {
            done();
          }else {
            done('Still inserted');
          }
        }).catch(e => {
          done();
        })
      });
      it('Year is less than 209', function (done) {
        connection.stranger.insert({...stranger, Year: 2018}).then(rs => {
          if (!rs.status) {
            done();
          }else {
            done('Still inserted');
          }
        }).catch(e => {
          done(e);
        })
      });

      it('Month is not number', function (done) {
        connection.stranger.insert({...stranger, Month: null}).then(rs => {
          if (!rs.status) {
            done();
          }else {
            done('Still inserted');
          }
        }).catch(e => {
          done();
        })
      });
      it('Month is greated than 12', function (done) {
        connection.stranger.insert({...stranger, Month: 13}).then(rs => {
          if (!rs.status) {
            done();
          }else {
            done('Still inserted');
          }
        }).catch(e => {
          done(e);
        })
      });
      it('Month is less than 1', function (done) {
        connection.stranger.insert({...stranger, Month: 0}).then(rs => {
          if (!rs.status) {
            done();
          }else {
            done('Still inserted');
          }
        }).catch(e => {
          done(e);
        })
      });

      it('Day is not number', function (done) {
        connection.stranger.insert({...stranger, Day: fastGenText(13)}).then(rs => {
          if (!rs.status) {
            done();
          }else {
            done('Still inserted');
          }
        }).catch(e => {
          done();
        })
      });
      it('Day is greater than 31', function (done) {
        connection.stranger.insert({...stranger, Day: 32}).then(rs => {
          if (!rs.status) {
            done();
          }else {
            done('Still inserted');
          }
        }).catch(e => {
          done(e);
        })
      });
      it('Day is less than 1', function (done) {
        connection.stranger.insert({...stranger, Day: 0}).then(rs => {
          if (!rs.status) {
            done();
          }else {
            done('Still inserted');
          }
        }).catch(e => {
          done(e);
        })
      });

      it('Hour is less than 0', function (done) {
        connection.stranger.insert({...stranger, Hour: -1}).then(rs => {
          if (!rs.status) {
            done();
          }else {
            done('Still inserted');
          }
        }).catch(e => {
          done();
        })
      });
      it('Hour is greater than 23', function (done) {
        connection.stranger.insert({...stranger, Hour: 24}).then(rs => {
          if (!rs.status) {
            done();
          }else {
            done('Still inserted');
          }
        }).catch(e => {
          done(e);
        })
      });
      it('Hour is not a number', function (done) {
        connection.stranger.insert({...stranger, Hour: 24}).then(rs => {
          if (!rs.status) {
            done();
          }else {
            done('Still inserted');
          }
        }).catch(e => {
          done(e);
        })
      });

      it('Minute is not a number', function (done) {
        connection.stranger.insert({...stranger, Minute: 'asd'}).then(rs => {
          if (!rs.status) {
            done();
          }else {
            done('Still inserted');
          }
        }).catch(e => {
          done();
        })
      });
      it('Minute is greater than 59', function (done) {
        connection.stranger.insert({...stranger, Minute: 60}).then(rs => {
          if (!rs.status) {
            done();
          }else {
            done('Still inserted');
          }
        }).catch(e => {
          done(e);
        })
      });
      it('Minute is less than 0', function (done) {
        connection.stranger.insert({...stranger, Minute: -1}).then(rs => {
          if (!rs.status) {
            done();
          }else {
            done('Still inserted');
          }
        }).catch(e => {
          done(e);
        })
      });

      it('Second is not a number', function (done) {
        connection.stranger.insert({...stranger, Second: 'asd'}).then(rs => {
          if (!rs.status) {
            done();
          }else {
            done('Still inserted');
          }
        }).catch(e => {
          done();
        })
      });
      it('Second is greater than 59', function (done) {
        connection.stranger.insert({...stranger, Minute: 60}).then(rs => {
          if (!rs.status) {
            done();
          }else {
            done('Still inserted');
          }
        }).catch(e => {
          done(e);
        })
      });
      it('Second is less than 0', function (done) {
        connection.stranger.insert({...stranger, Minute: -1}).then(rs => {
          if (!rs.status) {
            done();
          }else {
            done('Still inserted');
          }
        }).catch(e => {
          done(e);
        })
      });

    });

    describe('Stranger Exist', () => {
      let count = 0;
      before(async () => {
        count = await connection.stranger.countAll();
      });

      it('should increase by 1 when create an stranger successfully', function (done) {
        connection.stranger.insert(stranger).then((rs) => {
          console.log(rs)
          connection.stranger.countAll().then(size => {
            if (size === count + 1) {
              done();
            } else {
              done(`${size}, ${count}`);
            }
          }).catch(e2 => {
            done(e2);
          })
        }).catch(e => {
          done(e);
        });
      });
    })

  });

});