var assert = require('assert'), config = require('../config/dev'), db = require('../model/Gateway/InitialConnect');
let chai = require('chai');
let example = require('./FactoryGirl'), moment = require('moment');
let expect = chai.expect;

function fastGenText(number, postfix = '') {
  let string = '';
  for (let i = 0; i < number; i++) {
    string += 'a';
  }
  return string + postfix;
}

describe('User Connection', function () {
  let connection = new db(config);
  let user = example.user;
  after(() => {
    connection.user.query(`Delete from user where UserName = '${user.UserName}'`);
  })
  describe('Create User', () => {

    describe('should be unsuccessful', function () {

      it('UserName length is overlength 21', function (done) {
        connection.user.insert({...user, UserName: fastGenText(21)}).then(rs => {
          done('fails')
        }).catch(e => {
          done();
        })
      });

      it('UserName length is underlength 21', function (done) {
        connection.user.insert({...user, UserName: fastGenText(4)}).then(rs => {
          done('fails')
        }).catch(e => {
          done();
        })
      });

      it('UserName length is null', function (done) {
        connection.user.insert({...user, UserName: null}).then(rs => {
          done('fails')
        }).catch(e => {
          done();
        })
      });

      it('PassWord length is overlength 21', function (done) {
        connection.user.insert({...user, PassWord: fastGenText(21)}).then(rs => {
          done('fails')
        }).catch(e => {
          done();
        })
      });

      it('PassWord length is underlength 4', function (done) {
        connection.user.insert({...user, PassWord: fastGenText(4)}).then(rs => {
          done('fails')
        }).catch(e => {
          done();
        })
      })

      it('PassWord length is null', function (done) {
        connection.user.insert({...user, PassWord: null}).then(rs => {
          done('fails')
        }).catch(e => {
          done();
        })
      });

      it('Dob length is not date', function (done) {
        connection.user.insert({...user, Dob: fastGenText(21)}).then(rs => {
          done('fails')
        }).catch(e => {
          done();
        })
      });

      it('FristName length is 0', function (done) {
        connection.user.insert({...user, FirstName: ''}).then(rs => {
          done('fails')
        }).catch(e => {
          done();
        })
      });

      it('FristName length is 21', function (done) {
        connection.user.insert({...user, FirstName: fastGenText(21)}).then(rs => {
          done('fails')
        }).catch(e => {
          done();
        })
      });

      it('MobileNumber length is 31', function (done) {
        connection.user.insert({...user, MobileNumber: fastGenText(31)}).then(rs => {
          done('fails')
        }).catch(e => {
          done();
        })
      });

      it('Email length is 11', function (done) {
        connection.user.insert({...user, Email: fastGenText(1, '@gmail.com')}).then(rs => {
          done('fails')
        }).catch(e => {
          done();
        })
      });

      it('Email length is over 120', function (done) {
        connection.user.insert({...user, Email: fastGenText(120, '@gmail.com')}).then(rs => {
          done('fails')
        }).catch(e => {
          done();
        })
      });

    });

    describe('User Exist', () => {
      let second_user = {
        UserName: 'randomtest2',
        PassWord: "123456789",
        Dob: moment().format('YYYY-MM-DD'),
        FirstName: 'Minhthuc',
        LastName: 'Truong',
        salt: 'zdascaswdas',
        MobileNumber: '12312445654',
        Email: "minhthuc224@gmail.com",
        AccessToken: "askjdhowidklasjiw"
      }
      let count = 0;
      before(async () => {
        count = await connection.user.countAll();
      });

      it('should increase by 1 when create a user successfully', function (done) {
        connection.user.insert(user).then(() => {
          connection.user.countAll().then(size => {
            if (size === count + 1) {
              done();
            } else {
              done('false');
            }
          }).catch(e2 => {
            done(e2);
          })
        }).catch(e => {
          done(e);
        });
      });

      it('should not be successful when duplicate a user', done => {
        connection.user.insert(user).then(rs => {
          done('fails');
        }).catch(e => {
          done()
        })
      });

      it('should not be successful when email exist', done => {
        connection.user.insert({...second_user, Email: user.Email}).then(rs => {
          done('fails');
        }).catch(e => {
          done()
        })
      });

      it('should not be successful when MobileNumber exist', done => {
        connection.user.insert({...second_user, MobileNumber: user.MobileNumber}).then(rs => {
          done('fails');
        }).catch(e => {
          done()
        })
      });

      it('should not be successful when UserName exist', done => {
        connection.user.insert({...second_user, UserName: user.UserName}).then(rs => {
          done('fails');
        }).catch(e => {
          done()
        })
      });

      it('should include user at first', function (done) {
        connection.user.listUsers(10, 0).then(rs => {
          let exist = rs.data.find(r => r.UserName === user.UserName);
          if (exist && rs.data[0].UserName === user.UserName) {
            done()
          } else {
            done('fails');
          }
        })
      });

      it('should have exactly count', function (done) {
        connection.user.listUsers(10, 0).then(rs => {
          if (rs.size === (count + 1)) {
            done();
          }else{
            done("false");
          }
        })
      });

      it('should be find user successfully', function (done) {
        connection.user.findUserName(user.UserName).then(u=>{
          if (u){
            done();
          }else {
            done('fails')
          }
        })
      });

    })

  });

});